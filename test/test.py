import unittest

import numpy as np

from classifiers.mwknn import MultiWindowKnn
from classifiers.ourknn import OurKnn
from classifiers.trckd import TRCKD

from automatic_identification import likelihood_ratio


class TestLikelihoodRatio(unittest.TestCase):

    def test1(self):
        par = np.array([1, 2, 3, 5, 7])
        chil = np.array([1, 2, 3, 4, 6, 8, 9])
        other = [10, 11, 13, 20, 21, 22, 7, 2]
        all_ex = np.unique(np.concatenate((other, par, chil)))

        value = likelihood_ratio(par, chil, all_ex)

        self.assertAlmostEquals(value, 1.71428, 4)

    def test2(self):
        chil = np.array([1, 2, 3, 5, 7])
        par = np.array([1, 2, 3, 4, 6, 8, 9])
        other = [10, 11, 13, 20, 21, 22, 7, 2]
        all_ex = np.unique(np.concatenate((other, par, chil)))

        value = likelihood_ratio(par, chil, all_ex)

        self.assertAlmostEquals(value, 1.4999999, 4)


class TestClassifierClass(unittest.TestCase):

    def setUp(self) -> None:
        self.knn = Classifier(n_p=23, n_n=40, k=11, threshold_window=100,
                              min_label_threshold=0.5)

    def test_init_clf(self):
        self.assertEqual(self.knn.n_p, 23)
        self.assertEqual(self.knn.n_n, 40)

    def test_add_sample(self):
        xt = np.array([1, 2, 3])
        yt = {'label1', 'label2'}
        self.knn.fit(xt, yt)

        self.assertEqual(len(self.knn.Qp.keys()), 2)
        self.assertEqual(len(self.knn.Qn.keys()), 2)
        self.assertEqual(len(self.knn.Qp['label1']), 1)
        self.assertEqual(len(self.knn.Qn['label1']), 0)
        self.assertEqual(set(self.knn.classes), yt)
        self.assertEqual(self.knn.sample_counter.loc[0], 2)
        self.assertEqual(self.knn.total_sample_counter, 1)

    def test_add_more_samples(self):
        xt = np.array([1, 2, 3])
        yt = {'label1', 'label2'}
        self.knn.fit(xt, yt)
        xt = np.array([2, 2, 3])
        yt = {'label1', 'label3', 'label4'}
        self.knn.fit(xt, yt)

        self.assertEqual(len(self.knn.Qp.keys()), 4)
        self.assertEqual(len(self.knn.Qn.keys()), 4)
        self.assertEqual(len(self.knn.Qp['label1']), 2)
        self.assertEqual(len(self.knn.Qn['label2']), 1)
        self.assertEqual(len(self.knn.Qn['label1']), 0)
        self.assertEqual(set(self.knn.classes),
                         {'label1', 'label2', 'label3', 'label4'})
        self.assertEqual(self.knn.sample_counter.loc[0], 2)
        self.assertEqual(self.knn.sample_counter.loc[1], 4)
        self.assertEqual(self.knn.total_sample_counter, 2)

    def test_predict(self):
        xt = np.array([1, 2, 3])
        yt = {'label1', 'label2'}
        self.knn.fit(xt, yt)

        x = np.array([1, 1, 1])
        y_pred = self.knn.predict(x)
        print(y_pred)

    def test_thresholds_updates(self):
        self.knn.label_frequency['label1'] = 30
        self.knn.label_frequency['label2'] = 5
        self.knn.label_frequency['label3'] = 90

        self.knn.label_confidence['label1'] = [0.1, 0.35, 0.4, 0.5]
        self.knn.label_confidence['label2'] = [0.1, 0.35, 0.4, 0.5]
        self.knn.label_confidence['label3'] = [0.1, 0.35, 0.4, 0.7]

        self.assertEqual(0.5, self.knn.label_threshold['label1'])
        self.assertEqual(0.5, self.knn.label_threshold['label2'])
        self.assertEqual(0.5, self.knn.label_threshold['label3'])

        self.knn._compute_threshold()

        self.assertEqual(0.22, self.knn.label_threshold['label1'])
        self.assertEqual(0.2, self.knn.label_threshold['label2'])
        self.assertEqual(0.7, self.knn.label_threshold['label3'])


class TestOurMethod(unittest.TestCase):

    def setUp(self) -> None:
        self.clf = TRCKD(n_p=23, n_n=40, k=11, threshold_window=100,
                             min_label_threshold=0.5)
        for _, label in zip(range(5),
                            ['label1', 'label1', 'label2', 'label3', 'label4']):
            xt = np.array([1, 2, 3])
            yt = {label}
            self.clf.fit(xt, yt)

    def test_adaption_to_removed_class(self):
        self.assertEqual(4, len(self.clf.classes))

        self.clf.delete_class('label1')

        self.assertEqual(3, len(self.clf.classes))
        self.assertFalse('label1' in list(self.clf.Qp.keys()))
        self.assertFalse('label1' in list(self.clf.Qn.keys()))

    def test_add_relation(self):
        self.assertEqual(2, len(self.clf.Qp['label1']))
        self.assertEqual(1, len(self.clf.Qp['label2']))

        self.clf.add_relation('label1', 'label2')

        self.assertEqual(2, len(self.clf.Qp['label1']))
        self.assertEqual(3, len(self.clf.Qp['label2']))

    def test_delete_relation(self):
        self.clf.add_relation('label1', 'label2')

        self.clf.delete_relation('label1', 'label2')

        self.assertEqual(2, len(self.clf.Qp['label1']))
        self.assertEqual(1, len(self.clf.Qp['label2']))


class TestOurKnn(unittest.TestCase):

    def setUp(self) -> None:
        self.clf = OurKnn(k=3, window_size=10)
        for _, label in zip(range(5),
                            ['label1', 'label1', 'label2', 'label3', 'label4']):
            xt = np.array([1, 2, 3])
            yt = {label}
            self.clf.fit(xt, yt)

    def test_removed_class(self):
        pass

    def test_add_relation(self):
        pass

    def test_remove_relation(self):
        pass
