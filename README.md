# Human-in-the-loop Handling of Knowledge Drift

This repository contains the code for reproducing the experiments in the paper:

Andrea Bontempelli, Fausto Giunchiglia, Andrea Passerini and Stefano Teso. 
_Human-in-the-loop Handling of Knowledge Drift_. 2021.

[Paper on ArXiv.org](https://arxiv.org/abs/2103.14874)

[Slides](https://doi.org/10.5281/zenodo.5511836)

![](opendrift.png)

## Abstract

We introduce and study knowledge drift (KD), a special form of concept drift that
occurs in hierarchical classification. Under KD the vocabulary of concepts, their
individual distributions, and the is-a relations between them can all change over time. 
The main challenge is that, since the ground-truth concept hierarchy is unobserved, it
is hard to tell apart different forms of KD.  For instance, the introduction of a new is-a
relation between two concepts might be confused with changes to those individual
concepts, but it is far from equivalent. Failure to identify the right kind of KD
compromises the concept hierarchy used by the classifier, leading to systematic
prediction errors.  Our key observation is that in human-in-the-loop applications like
smart personal assistants the user knows what kind of drift occurred recently, if any. 
Motivated by this observation, we introduce TRCKD, a novel approach that combines
two automated stages -- drift detection and adaptation -- with a new interactive
disambiguation stage in which the user is asked to refine the machine's understanding
of recently detected KD. In addition,TRCKD implements a simple but effective
knowledge-aware adaptation strategy. Our simulations show that, when the structure
of the concept hierarchy drifts, a handful of queries to the user are often enough to
substantially improve prediction performance on both synthetic and realistic data.

## Getting started

### Prerequisites

- python >= 3.6

Older versions may also work.

### Installation

Clone the repository

```bash
$ git clone https://gitlab.com/abonte/handling-knowledge-drift.git
$ cd handling-knowledge-drift
```

Create a new environment

```bash
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```


### Run

Modify the configuration file `src/config.yaml`.

To run an experiment see `src/main.py`:

```
usage: main.py [-h] -pos DRIFT_POSITIONS [DRIFT_POSITIONS ...] [-p P]
               [-config CONFIG]
               [-drift {add_implication,all_drifts,concept_drift,remove_concept,remove_implication}]
               [-R N_REPEATS] [-I MAX_ITERS] [-T TEST_SIZE] [-k P_KNOWN]
               [--param-selection]
               experiment {20ng,emnist,hstagger}

positional arguments:
  experiment            name of the experiment
  {20ng,emnist,hstagger}
                        name of the dataset

optional arguments:
  -h, --help            show this help message and exit
  -pos DRIFT_POSITIONS [DRIFT_POSITIONS ...], --drift-positions DRIFT_POSITIONS [DRIFT_POSITIONS ...]
                        iterations where the drifts will occur (default: None)
  -p P                  # of parallel processes (default: 8)
  -config CONFIG        configuration file for the methods (default:
                        src/config.yaml)
  -drift {add_implication,all_drifts,concept_drift,remove_concept,remove_implication}

Evaluation:
  -R N_REPEATS, --n-repeats N_REPEATS
                        # of times the experiment is repeated (default: 8)
  -I MAX_ITERS, --max-iters MAX_ITERS
                        # of interaction rounds (default: 200)
  -T TEST_SIZE, --test-size TEST_SIZE
                        # of examples in the test set (default: 200)
  -k P_KNOWN, --p-known P_KNOWN
                        # of initially known training examples (default: 11)
  --param-selection     perform two run for hyperparameter selection (default:
                        False)
```

To generate the plots see `src/draw.py`:

```
usage: draw.py [-h] exp_name

positional arguments:
  exp_name    name of the experiment folder

optional arguments:
  -h, --help  show this help message and exit
```

## Repository structure 

|Filename / Folder name| Description   |
|---|---|
|`src/configs`| configuration files used for the experiments|
|`src/classifiers`| implementation of the proposed method (`trckd.py`) and the competitors|
|`src/data`| the stream generators for the three data sets (EMNIST, HSTAGGER, 20NG)|
|`src/visualization`| scripts to create plots|


## Reproduce experiments in the paper

To run the experiments with single drift, replace `<DATASET>` with `20`, `hstagger` or `emnist`
and run the following command:

```bash
$ ./run_paper.sh <DATASET>
```

To run the experiments with multiple drift for all datasets:

```bash
$ ./run_paper.sh sequential_drifts
```

Experiments with Graphical Lasso on hstagger:

```bash
$ ./run_paper.sh glasso
```

To plot the results as reported in the paper, substitute `<DATASET>` with one of
[`20ng`, `hstagger`, `emnist`, `glasso`, `glasso`]: 

```bash
$ ./plot_paper.sh <DATASET>
```
The plots will be saved in the `report` folder.


To generate the plots for as single experiment, run the following command for each dataset and type of drift
by replacing `<FOLDER>` with the corresponding folder in `results`:

```bash
$ python src/draw.py <FOLDER>
```

For example, run `python src/draw.py mmd__dataset=emnist__drift=remove_implication`.
The plots are stored in the corresponding experiment folder in `results`.



## Cite

```markdown
@article{bontempelli2021humanintheloop,
      title={Human-in-the-loop Handling of Knowledge Drift}, 
      author={Andrea Bontempelli and Fausto Giunchiglia and Andrea Passerini and Stefano Teso},
      year={2021},
      eprint={2103.14874},
      archivePrefix={arXiv},
      primaryClass={cs.LG}
}
```

## Datasets

**EMNIST**

Cohen, Gregory, et al. "EMNIST: Extending MNIST to handwritten letters." 2017 International Joint Conference on Neural Networks (IJCNN). IEEE, 2017.

**Twenty Newsgroups**

Dua, D. and Graff, C. (2019). UCI Machine Learning Repository [http://archive.ics.uci.edu/ml]. Irvine, CA: University of California, School of Information and Computer Science. 

## Acknowledgments

The research of FG and AP has received funding from the European Union's Horizon 2020
FET Proactive project "WeNet -- The Internet of us", grant agreement No 823783.
The research of AB and ST has received funding from the 
"DELPhi - DiscovEring Life Patterns" project funded by the MIUR Progetti di Ricerca di 
Rilevante Interesse Nazionale (PRIN) 2017 -- DD n. 1062 del 31.05.2019.
