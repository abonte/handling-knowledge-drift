#!/bin/bash

if [[ $1 = 'hstagger' ]]; then
    shared="hstagger -k 11 -I 275 -pos 100"
    python3 src/main.py mmd $shared -drift add_implication -config src/configs/hstagger.yaml
    python3 src/main.py mmd $shared -drift remove_implication -config src/configs/hstagger.yaml
    python3 src/main.py mmd $shared -drift remove_concept -config src/configs/hstagger.yaml
    python3 src/main.py mmd $shared -drift concept_drift -config src/configs/hstagger_cd.yaml

elif [[ $1 = 'emnist' ]]; then
    shared="emnist -k 11 -I 275 -pos 100 -T 200"
    python3 src/main.py mmd $shared -drift add_implication -config src/configs/emnist.yaml
    python3 src/main.py mmd $shared -drift remove_implication -config src/configs/emnist_ri.yaml
    python3 src/main.py mmd $shared -drift remove_concept -config src/configs/emnist.yaml
    python3 src/main.py mmd $shared -drift concept_drift -config src/configs/emnist_cd.yaml

elif [[ $1 = '20ng' ]]; then
    shared="20ng -k 11 -I 300 -pos 170 -T 200"
    python3 src/main.py mmd $shared -drift add_implication -config src/configs/20ng.yaml
    python3 src/main.py mmd $shared -drift remove_implication -config src/configs/20ng.yaml
    python3 src/main.py mmd $shared -drift remove_concept -config src/configs/20ng.yaml
    python3 src/main.py mmd $shared -drift concept_drift -config src/configs/20ng_cd.yaml

elif [[ $1 = 'sequential_drifts' ]]; then
    python3 src/main.py mmd hstagger -k 11 -I 570 -pos 120 220 320 450 -T 200 -drift all_drifts -config src/configs/hstagger_alldrifts.yaml
    python3 src/main.py mmd emnist -k 11 -I 570 -pos 180 280 380 480 -T 200 -drift all_drifts -config src/configs/emnist_alldrifts.yaml
    python3 src/main.py mmd 20ng -k 11 -I 570 -pos 180 280 380 480 -T 200 -drift all_drifts -config src/configs/20ng_alldrifts.yaml

elif [[ $1 = 'glasso' ]]; then
    shared="hstagger -k 11 -I 275 -pos 100 -T 200"
    python3 src/main.py glasso $shared -drift add_implication -config src/configs/hstagger_glasso.yaml
    python3 src/main.py glasso $shared -drift remove_implication -config src/configs/hstagger_glasso.yaml

fi
