#!/bin/bash

if [[ $1 = 'hstagger' || $1 = 'emnist' || $1 = '20ng' ]]; then

    for drift in concept_drift remove_concept add_implication remove_implication; do

        folder=`ls -d results/mmd__dataset=${1}__drift=${drift}*`
        cmd="python src/plots_for_paper.py $1 -i $folder -o reports -rq rq1 rq2"
        echo $cmd; $cmd

    done

elif [[ $1 = 'sequential_drifts' ]]; then

    for dataset in hstagger emnist 20ng; do
      folder=`ls -d results/mmd__dataset=${dataset}__drift=all_drifts*`
      cmd="python src/plots_for_paper.py $dataset -i $folder -o reports -rq rq3"
      echo $cmd; $cmd
    done

elif [[ $1 = 'glasso' ]]; then

    for drift in add_implication remove_implication; do
      folder=`ls -d results/glasso__dataset=hstagger__drift=${drift}*`
      cmd="python src/plots_for_paper.py hstagger -i $folder -o reports -rq lasso"
      echo $cmd; $cmd
    done
fi