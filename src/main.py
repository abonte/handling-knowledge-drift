import argparse
import copy
import logging
import multiprocessing as mp
import os

import numpy as np
from sklearn.model_selection import ParameterGrid

from data.stream_generator import TypeOfDrift
from execution import execution, DATASETS
from utils import save_as_json, save_as_pickle, load_config
from visualization.visualize import plot_all_statistics
from visualization.visualize_average import compute_average

logging.basicConfig(level=logging.INFO)


def _get_basename(args):
    fields = [
        ('dataset', args.dataset),
        ('drift', args.drift_type),
        ('pos', '_'.join(str(e) for e in args.drift_positions)),
        ('R', args.n_repeats),
        ('iters', args.max_iters),
        ('k', args.p_known),
        ('T', args.test_size),
        ('psel', args.param_selection)
    ]

    basename = args.experiment + '__' + '__'.join([f'{name}={str(value)}'
                                                   for name, value in fields])
    return basename


def save_stat(stat, config, args, save_path, iter_name):
    classes_support = {k: str(v) for k, v in
                       zip(stat['all_labels'], np.sum(stat['y_true'], axis=0))}
    dataset_stats = {
        '|D|': args.max_iters,
        '|LC|': np.mean(stat["cardinality"]),
        'LD': round(np.mean(stat["cardinality"]) / len(stat["all_labels"]), 2),
        'dis': len(stat["label_sets"]),
        'n_drifts': len(stat["drift_history"].keys()),
        'classes': classes_support
    }
    save_as_json(dataset_stats,
                 os.path.join(save_path, iter_name + '_dataset_stats.json'))

    # ********* to avoid error with pickle
    stat['metrics'] = dict(stat['metrics'])
    stat['mmd_per_label'] = dict(stat['mmd_per_label'])
    stat['kernel_param_per_label'] = dict(stat['kernel_param_per_label'])
    # *********

    save_as_pickle(stat, os.path.join(save_path, iter_name + '_iteration_stats.pickle'))


def _run(save_path, repeat_counter, seed, config, args, param_selection_method=None):
    config['seed'] = seed

    stat = execution(config, args, repeat_counter)

    if param_selection_method is not None:
        iter_name = f'param_selection_{param_selection_method}_{repeat_counter}'
    else:
        iter_name = str(seed)
    save_stat(stat, config, args, save_path, iter_name)
    plot_all_statistics(stat, save_path, iter_name)
    return stat


def main(args):
    np.seterr(all='raise')

    logging.info('Read configuration file')
    config = load_config(args.config)

    basename = _get_basename(args)
    save_path = os.path.join('results', basename)
    os.makedirs(save_path)

    logging.info(f'Start "{basename}"')
    p = mp.Pool(args.p)

    run_counter = 0
    if args.param_selection:
        logging.info('Start parameters selection')
        # parameter selection k of PAW-knn, k of TRCKD-based methods, MMD threshold
        k_punitive, k_other, thres, run_counter = parameter_selection(config, save_path,
                                                                      p, args)
        config['punitive_knn']['k'] = k_punitive
        config['k'] = k_other
        config['drift_detector']['threshold'] = thres

    logging.info('Start experiments')

    _ = p.starmap_async(func=_run,
                        iterable=[(save_path, run_counter + cv_round, cv_round, config, args)
                                  for cv_round in
                                  range(2, args.n_repeats + 2)] # 2 are the run for param selection
                        ).get()

    p.close()
    p.join()

    save_as_json(config, os.path.join(save_path, 'config.json'))
    logging.info('Done!')


def parameter_selection(config, save_path, p, args):
    k_punitive, k_other, threshold = config['punitive_knn']['k'], config['k'], \
                                     config['drift_detector']['threshold']

    params = {'ourmethod': {'k': [3, 5, 11],
                            'threshold': [.04, .05]},
              'punitive_knn': {'k': [3, 5, 11]}
              }

    configurations = dict()
    for method, params in params.items():
        if method not in config['methods']:
            continue
        configurations[method] = list()
        for i, param_grid in enumerate(ParameterGrid(params)):
            tmp_config = copy.deepcopy(config)
            tmp_config['methods'] = [method]

            if 'ourmethod' == method:
                tmp_config['k'] = param_grid['k']
                tmp_config['drift_detector']['threshold'] = param_grid['threshold']
            elif 'punitive_knn' == method:
                tmp_config['punitive_knn']['k'] = param_grid['k']

            configurations[method].append(tmp_config)

    runs = dict()
    run_counter = 0
    for method, run_configs in configurations.items():
        runs[method] = list()
        for par in run_configs:
            runs[method].append(p.starmap_async(func=_run, iterable=[
                (save_path, run_counter + i, i, par, args, method)
                for i in range(2)]))
            run_counter += 2

    for method in runs.keys():
        average_result = list()
        for elem in runs[method]:
            res = elem.get()
            average_result.append(compute_average(res))

        best_exec = 0
        for it in range(1, len(average_result)):
            best_exec = _find_best(best_exec, average_result[best_exec], it,
                                   average_result[it], method)

        if 'ourmethod' == method:
            k_other = configurations[method][best_exec]['k']
            threshold = configurations[method][best_exec]['drift_detector']['threshold']
        elif 'punitive_knn' == method:
            k_punitive = configurations[method][best_exec]['punitive_knn']['k']

    logging.info(
        f'Params: k punitive_knn: {k_punitive}, k ourmethod: {k_other}, thres: {threshold}')
    return k_punitive, k_other, threshold, run_counter


def _find_best(key_1, average1, key_2, average2, method):
    f11 = average1['f1'][method]['average']
    f12 = average2['f1'][method]['average']
    vote1, vote2 = 0, 0
    for i in range(len(f11)):
        if f11[i] > f12[i]:
            vote1 += 1
        elif f11[i] < f12[i]:
            vote2 += 1
    return key_1 if vote1 > vote2 else key_2


if __name__ == '__main__':
    fmt_class = argparse.ArgumentDefaultsHelpFormatter
    parser = argparse.ArgumentParser(formatter_class=fmt_class)
    parser.add_argument('experiment', help='name of the experiment')
    parser.add_argument('dataset', choices=sorted(DATASETS.keys()),
                        help='name of the dataset')
    parser.add_argument('-pos', '--drift-positions', type=int, nargs='+', required=True,
                        help="iterations where the drifts will occur")
    parser.add_argument('-p', type=int, default=mp.cpu_count(),
                        help='# of parallel processes')
    parser.add_argument('-config', type=str, default='src/config.yaml',
                        help='configuration file for the methods')
    parser.add_argument('-drift', choices=sorted([e.value for e in TypeOfDrift]),
                        dest='drift_type')

    group = parser.add_argument_group('Evaluation')
    group.add_argument('-R', '--n-repeats', type=int, default=8,
                       help='# of times the experiment is repeated')
    group.add_argument('-I', '--max-iters', type=int, default=200,
                       help='# of interaction rounds')
    group.add_argument('-T', '--test-size', type=int, default=200,
                       help='# of examples in the test set')
    group.add_argument('-k', '--p-known', type=int, default=11,
                       help='# of initially known training examples')
    group.add_argument('--param-selection', default=False,
                       action='store_true',
                       help='perform two run for hyperparameter selection')

    args = parser.parse_args()
    main(args)
