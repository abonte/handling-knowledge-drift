import math
from collections import defaultdict

from sklearn.multioutput import MultiOutputClassifier
from sklearn.neighbors import KNeighborsClassifier

from classifiers.utils import sort
import pandas as pd
import numpy as np


class PunitiveKnn:
    """
    MLSAMPkNN

    Reference:
    ----------

    Martha Roseberry, Bartosz Krawczyk, and Alberto Cano. 2019. Multi-label punitive kNN
    with self-adjusting memory for drifting data streams. ACM Transactions on Knowledge
    Discovery from Data (TKDD) 13, 6 (2019), 1–31.
    """

    def __init__(self, k: int, penalty_ration: float, m_min: int, m_max: int):
        self.k = k
        self.penalty_ration = penalty_ration
        self.m_min = m_min
        self.m_max = m_max
        self.X_window = None
        self.y_window = pd.DataFrame(dtype=bool)
        self.y_pred_window = pd.DataFrame(dtype=bool)
        self.error = pd.Series(dtype=int)
        self.classes = set()
        self.sample_idx = 0
        self.knn = MultiOutputClassifier(
            KNeighborsClassifier(n_neighbors=self.k, n_jobs=1))

    def fit_one(self, xt, yt):
        xt_index = self.sample_idx
        self.sample_idx += 1

        if self.X_window is not None and len(self.X_window) > 10:
            votes = defaultdict(lambda: 0)
            for index, _ in self.neighbors(xt, self.k).iteritems():
                neighbor_labels = [col for v, col in zip(self.y_window.loc[index].tolist(), self.y_window.columns) if v]
                for label in self.classes:
                    if label in neighbor_labels:
                        votes[label] += 1
                    if (label in yt and label not in neighbor_labels) or \
                            (label not in yt and label in neighbor_labels):
                        self.error[index] += 1

            predicted_labels = set()
            for label in self.classes:
                if votes[label] / self.k > 0.5:
                    predicted_labels.add(label)
        else:
            predicted_labels = set()

        self.add_sample(xt, yt, xt_index)

        condition = self.error > len(self.classes) * self.penalty_ration
        idx_to_remove = self.error[condition].index
        self._remove_samples(idx_to_remove)

        m = len(self.X_window)
        if m / 2 > self.m_min:
            subset_accuracy = dict()
            c = 1
            m_prime = m
            while m_prime > self.m_min:
                subset_accuracy[m_prime] = self.evaluate_size(m_prime)
                m_prime = math.ceil(m / (c * 2))
                c *= 2

            self.adjust_size(max(subset_accuracy, key=subset_accuracy.get))

        if len(self.X_window) > self.m_max:
            self.remove_oldest()

        self.knn.fit(self.X_window, self.y_window)

    def predict_one(self, xt):
        if len(self.X_window) >= self.k:
            xt = np.atleast_2d(xt)
            y_encoded = self.knn.predict(xt)[0]
            return {cl_name for is_predicted, cl_name in zip(y_encoded, self.y_window.columns) if
                    is_predicted}
        else:
            return set()

    def neighbors(self, xt, k):
        sorted_wind = sort(self.X_window, xt)
        return sorted_wind[:k]

    def add_sample(self, xt, yt, xt_index):
        if self.X_window is None:
            self.X_window = pd.DataFrame(np.atleast_2d(xt))
        else:
            self.X_window.loc[xt_index] = xt

        new_classes = yt.difference(self.classes)
        for cl in new_classes:
            self.y_window[cl] = False
            self.y_pred_window[cl] = False
        self.classes = self.classes.union(yt)
        self.y_window.loc[xt_index] = [False] * len(self.classes)
        self.y_window.loc[xt_index, list(yt)] = True

        self.error.loc[xt_index] = 0

    def evaluate_size(self, m_prime):
        true_labels = self.y_window.iloc[-m_prime:]
        X_train = self.X_window.iloc[-m_prime:]
        knn = MultiOutputClassifier(KNeighborsClassifier(n_neighbors=self.k, n_jobs=1))
        knn.fit(X_train, true_labels)
        pred_labels = knn.predict(X_train)
        tmp_sum = np.sum(np.all(np.equal(true_labels, pred_labels), axis=1))
        return tmp_sum / m_prime

    def adjust_size(self, new_size):
        idx_to_remove = self.X_window.iloc[:-new_size].index
        self._remove_samples(idx_to_remove)

    def remove_oldest(self):
        n_sample_to_remove = len(self.X_window) - self.m_max
        idx_to_remove = self.X_window.iloc[:n_sample_to_remove].index
        self._remove_samples(idx_to_remove)

    def _remove_samples(self, idxs):
        self.X_window.drop(index=idxs, inplace=True)
        self.y_window.drop(index=idxs, inplace=True)
        self.y_pred_window.drop(index=idxs, inplace=True, errors='ignore')
        self.error.drop(index=idxs, inplace=True)
