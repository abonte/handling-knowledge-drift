import logging
from collections import deque, defaultdict

import numpy as np
import pandas as pd

from classifiers.me import ME
from classifiers.mmd import mmd_is_same_distribution
from classifiers.mwknn import MultiWindowKnn


class TRCKD(MultiWindowKnn):
    ADAPTATION = True

    def __init__(self, k,
                 classifier_config: dict,
                 detector_config: dict,
                 sample_to_keep: int = 0):

        super().__init__(k=k, **classifier_config)
        self.type_of_test = detector_config['test']
        self.test = {}
        if self.type_of_test == 'MMD':
            self._threshold = detector_config['threshold']
            assert self._threshold > 0
        elif self.type_of_test == 'ME':
            self.me_config = {'alpha': detector_config['alpha'],
                              'J': detector_config['J'],
                              'random_state': detector_config['random_state']}
        else:
            raise ValueError(self.type_of_test)
        assert sample_to_keep >= 0
        self.n_sample_to_keep = sample_to_keep
        self.window_drift_detector = detector_config['window']
        self.old_windows = defaultdict(lambda: None)

    def optimize(self):
        if self.type_of_test == 'MMD':
            return None
        dataset = self.buffer.copy(deep=True)
        for cl in self.classes:
            if cl == 'root':
                continue
            positive_sample = dataset.loc[self.Qp[cl]]
            positive_sample['y'] = 1
            negative_sample = dataset.loc[self.Qn[cl]]
            negative_sample['y'] = 0
            all_examples = pd.concat([positive_sample, negative_sample])
            all_examples = all_examples.sort_index()
            self.test[cl] = ME(**self.me_config)
            if len(all_examples) % 2 != 0:
                all_examples = all_examples.iloc[1:]

            split_index = int(len(all_examples) / 2)
            all_examples = all_examples.to_numpy()
            self.test[cl].optimize(all_examples[:split_index, :],
                                   all_examples[split_index:, :])

    def drift_detected(self, explanation=False):
        if self.type_of_test == 'ME' and len(self.test) == 0:
            # ME not optimized
            none_dict = {cl: 0 for cl in self.classes if cl != 'root'}
            result = {'is_drift_detected': False,
                      'stat_test_values': none_dict,
                      'kernel_params': none_dict,
                      'detected_classes': None}
            return result

        dataset = self.buffer.copy(deep=True)
        mmd_values = dict()
        is_same_dist = list()
        kernel_params = dict()
        classes_with_drift = list()
        explain_drift = dict()

        for cl in self.classes:
            if cl == 'root':
                continue
            if len(self.Qp[cl]) + len(self.Qn[cl]) < self.window_drift_detector \
                    and self.old_windows[cl] is None:
                # not enough data
                is_same_dist.append(True)
                mmd_values[cl] = 0
                kernel_params[cl] = 0
                continue

            positive_sample = dataset.loc[self.Qp[cl]]
            positive_sample['y'] = 1
            negative_sample = dataset.loc[self.Qn[cl]]
            negative_sample['y'] = 0
            new_window = pd.concat([positive_sample, negative_sample])
            new_window = new_window.sort_index().iloc[-self.window_drift_detector:, :]
            new_window = new_window.to_numpy()

            if len(new_window) < self.window_drift_detector:
                n_missing_examples = self.window_drift_detector - len(new_window)
                old_samples = self.old_windows[cl][-n_missing_examples:, :]
                if len(old_samples) < n_missing_examples:
                    logging.error(
                        f'different size {len(old_samples)}, {n_missing_examples}, {len(self.old_windows[cl])}')
                new_window = np.vstack([new_window, old_samples])

            if self.old_windows[cl] is None:
                self.old_windows[cl] = new_window
                same_dist, mmd, k_param = True, 0, 0
            else:
                min_wind = min(len(new_window), len(self.old_windows[cl]))
                if self.type_of_test == 'MMD':
                    same_dist, mmd, k_param = mmd_is_same_distribution(
                        self.old_windows[cl][-min_wind:], new_window[-min_wind:],
                        self._threshold)
                elif self.type_of_test == 'ME':
                    same_dist, mmd, k_param = self.test[cl].is_same_distribution(
                        self.old_windows[cl][-min_wind:], new_window[-min_wind:])

                if not same_dist:
                    if type(self.test) is dict and explanation:  # ME is used
                        exp = self.test[cl].explain_test(
                            self.old_windows[cl][-min_wind:], new_window[
                                                              -min_wind:])  # TODO check that are not modified in function
                        explain_drift[cl] = exp

                    self.old_windows[cl] = new_window
                    classes_with_drift.append(cl)

            is_same_dist.append(same_dist)
            mmd_values[cl] = mmd
            kernel_params[cl] = k_param

        result = {'is_drift_detected': not np.all(is_same_dist),
                  'stat_test_values': mmd_values,
                  'kernel_params': kernel_params,
                  'detected_classes': classes_with_drift}
        if explanation:
            result['explain_drift'] = explain_drift

        return result

    def delete_class(self, cl):
        try:
            self.classes.remove(cl)
            for queue in [self.Qp.pop(cl), self.Qn.pop(cl)]:
                self._empty_queue(queue)
            self._reset_label_thresholds(cl)
        except ValueError:
            logging.warn(f'{cl} not in clf')

    def adapt_to_concept_drift(self, cl):
        tmp_queue = deque(maxlen=self.Qp[cl].maxlen)
        if self.n_sample_to_keep > 0:
            sample_to_keep = list(self.Qp[cl])[-self.n_sample_to_keep:]
            for elem in sample_to_keep:
                self._update_windows(elem, tmp_queue)
        self._empty_queue(self.Qp[cl])
        if len(tmp_queue) > 0:
            self.Qp[cl] = tmp_queue
        self._empty_queue(self.Qn[cl])
        self._reset_label_thresholds(cl)

    def _empty_queue(self, queue):
        while len(queue) != 0:
            self._pop_element_from_queue(queue)

    def add_relation(self, parent, child):
        if child in self.classes and parent not in self.classes:
            self._add_new_class(parent)
        if child not in self.classes:
            logging.warn(f'Implication for {child} is not added.')
            return

        # resize positive and negative window size of cl_implied
        new_pos_window_size = self.Qp[parent].maxlen + self.n_p

        new_neg_window_size = round(
            new_pos_window_size * self.window_distribution_ratio)
        self.Qn[parent] = self.change_queue_max_len(self.Qn[parent],
                                                    new_neg_window_size)

        # merge to parent and child examples and sort
        indexes_in_window = TRCKD._merge_parent_and_child(self.Qp[child],
                                                          self.Qp[parent])

        tmp_queue = deque(maxlen=new_pos_window_size)
        for elem in indexes_in_window:
            self._update_windows(elem, tmp_queue)
            if elem in self.Qn[parent]:
                self.Qn[parent].remove(elem)
        self._empty_queue(self.Qp[parent])
        self.Qp[parent] = tmp_queue
        self._reset_label_thresholds(parent)

    def change_queue_max_len(self, queue, max_len):
        new_queue = deque(maxlen=max_len)
        if max_len == queue.maxlen:
            return queue
        elif max_len > queue.maxlen:
            new_queue.extend(queue)
        elif max_len < queue.maxlen:
            for elem in queue:
                self._update_windows(elem, new_queue)
            self._empty_queue(queue)
        return new_queue

    @staticmethod
    def _merge_parent_and_child(child_window, parent_window):
        child = list(child_window)
        parent = list(parent_window)
        parent.extend(child)
        parent = list(set(parent))
        parent.sort()
        return parent

    def increase_class_window_size(self, cl):
        new_pos_window_size = self.Qp[cl].maxlen + self.n_p
        new_neg_window_size = round(
            new_pos_window_size * self.window_distribution_ratio)
        self.Qp[cl] = self.change_queue_max_len(self.Qp[cl], new_pos_window_size)
        self.Qn[cl] = self.change_queue_max_len(self.Qn[cl], new_neg_window_size)

    def delete_relation(self, parent, child):
        if child not in self.classes or parent not in self.classes:
            logging.warn(f'Implication for {child} or {parent} is not removed.')
            return
        for elem in self.Qp[child]:
            if elem in self.Qp[parent]:
                self.Qp[parent].remove(elem)
                self._remove_sample_from_counter(elem)

        # resize windows size
        new_pos_window_size = max(self.Qp[parent].maxlen - self.n_p, self.n_p)
        self.Qp[parent] = self.change_queue_max_len(self.Qp[parent],
                                                    new_pos_window_size)

        new_neg_window_size = round(
            new_pos_window_size * self.window_distribution_ratio)
        self.Qn[parent] = self.change_queue_max_len(self.Qn[parent],
                                                    new_neg_window_size)

        self._reset_label_thresholds(parent)

    def _reset_label_thresholds(self, cl):
        # TODO check
        self.counter_to_update_threshold = 0
        self.label_frequency = defaultdict(int)
        self.label_confidence = defaultdict(list)
        # self.label_confidence.pop(cl, None)
        self.label_threshold.pop(cl, None)
        # self.label_frequency.pop(cl, None)
