import logging
import math

import freqopttest.util as util
import jax
import jax.numpy as jnp
import numpy as onp
import scipy.stats as stats
from jax.experimental import optimizers
from sklearn.model_selection import train_test_split


class ME:
    """
    Mean Embeddings

    Reference
    ---------

    Jitkrittum, Wittawat, et al. "Interpretable Distribution Features with Maximum
    Testing Power." NIPS. 2016.
    """

    def __init__(self, alpha, J, random_state):
        self.alpha = alpha
        self.J = J
        self.rng = onp.random.RandomState(random_state)
        self.gamma = None
        self.Tx = None
        self.Ty = None

    def is_same_distribution(self, old_window, new_window):
        test_result = perform_test(self.gamma, self.alpha, self.Tx, self.Ty, old_window,
                                   new_window)
        return not test_result['h0_rejected'], test_result['pvalue'], self.gamma

    def optimize(self, X1, X2):
        X3 = onp.vstack([X1, X2])
        # X3_pos = X3[X3[:, -1] == 1]
        # X3_neg = X3[X3[:, -1] == 0]
        # n_pos = min(math.ceil(self.J * 0.5), len(X3_pos))
        # n_neg = min(self.J - n_pos, len(X3_neg))
        # if len(X3_neg) == 0:
        #     n_pos = min(self.J, len(X3_pos))
        #     idx_neg = []
        # else:
        #     idx_neg = self.rng.choice(len(X3_neg), size=self.J - n_pos, replace=False)
        #
        # idx_pos = self.rng.choice(len(X3_pos), size=n_pos, replace=False)
        #
        #
        # logging.info(f'{n_pos}, {self.J - n_pos}')

        #examples = onp.vstack([X3_pos[idx_pos], X3_neg[idx_neg]])
        # T0_x, T0_y = examples[:, :-1], examples[:, -1]

        try:
            _, T0_x, _, T0_y = train_test_split(X3[:, :-1], X3[:, -1],
                                                test_size=self.J, random_state=self.rng)
        except ValueError:
            idx = self.rng.choice(len(X3), size=self.J, replace=True)
            T0_x, T0_y = X3[idx, :-1], X3[idx, -1]

        T0_x = T0_x.astype(onp.float32)
        # T0_y = T0_y[:, np.newaxis]

        gamma0 = util.meddistance(onp.vstack([X1[:, :-1], X2[:, :-1]]))

        params, S, gams = optimize_T_gaussian_width(X1, X2, T0_x, T0_y, gamma0, rng=self.rng)
        self.gamma = float(params[0]) if not math.isnan(float(params[0])) else gamma0
        self.Tx = params[1] if not onp.isnan(onp.min(params[1])) else T0_x
        self.Ty = T0_y


    def explain_test(self, X1, X2):
        def get_stats(X, Y, Tx, Ty, gamma, J):
            stats = onp.zeros(len(X))
            #idx = onp.random.choice(np.where(Y[:,-1] == 1)[0], J)
            #Tx, Ty = Y[idx, :-1], Y[idx, -1]
            for i in range(len(X)):
                locs_x = Tx[:-1].copy()
                locs_x = onp.vstack([locs_x, X[i, :-1]])
                locs_y = Ty[:-1].copy()
                locs_y = onp.hstack([locs_y, X[i, -1]])
                stats[i] = float(func([gamma, locs_x], locs_y, X, Y))
            return stats

        x1_stats = get_stats(X1, X2, self.Tx, self.Ty, self.gamma, self.J)
        indx1 = onp.argpartition(x1_stats, -5)[-5:]

        x2_stats = get_stats(X2, X1, self.Tx, self.Ty, self.gamma, self.J)
        indx2 = onp.argpartition(x2_stats, -5)[-5:]

        results = {'x1': X1[indx1],
                   'idx1': indx1,
                   'x2': X2[indx2],
                   'idx2': indx2}

        return results


def perform_test(gamma, alpha, locs_x, locs_y, X1, X2):
    stat = func([gamma, locs_x], locs_y, X1, X2)
    J = locs_x.shape[0]
    pvalue = stats.chi2.sf(stat, J)

    results = {'alpha': alpha,
               'pvalue': pvalue,
               'test_stat': stat,
               'h0_rejected': pvalue < alpha}
    return results


def optimize_T_gaussian_width(X1, X2, T0_x, T0_y, gamma0, step_size=1e-3,
                              max_iter=400, tol_fun=1e-3, rng=None):
    T0_x = jnp.array(T0_x)
    X1 = jnp.array(X1)
    X2 = jnp.array(X2)
    T0_y = jnp.array(T0_y)
    params = [gamma0, T0_x]
    nx = X1.shape[0]

    opt_init, opt_update, get_params = optimizers.adam(step_size)
    opt_state = opt_init(params)

    def step(it, opt_state):
        idx = rng.choice(nx, nx, replace=False)
        with jax.disable_jit():
            value, grads = jax.value_and_grad(func)(params, T0_y, X1[idx, :],
                                                    X2[idx, :])

        grads[0] *= -1
        grads[1] *= -1
        opt_state = opt_update(it, grads, opt_state)
        return value, opt_state

    S = onp.zeros(max_iter)
    gams = onp.zeros(max_iter)

    for it in range(max_iter):
        value, opt_state = step(it, opt_state)

        params = get_params(opt_state)

        S[it] = value
        gams[it] = params[0]

        if it >= 2 and abs(S[it] - S[it - 1]) <= tol_fun:
            break

    logging.info(f'ME opt: Epoch 0 {gams[0]} - Epoch {math.floor(max_iter/2)} {gams[math.floor(max_iter/2)]} - Epoch last {gams[-1]}')

    return params, S, gams


def func(params, locs_y, X1, X2):
    nx = X1.shape[0]
    Z = construct_z(X1, X2, params[1], locs_y, params[0])
    W = jnp.mean(Z, 0)
    Sig = jnp.cov(Z.T)
    s = jnp.linalg.solve(Sig, W).dot(nx * W)
    if jnp.isnan(s):
        logging.warn('isnan')
        Sig = Sig + 1e-8 * jnp.identity(len(Sig))
        s = jnp.linalg.solve(Sig, W).dot(nx * W)
        if jnp.isnan(s):
            logging.warn('isnan2')
    return s


def construct_z(X1, X2, locs_x, locs_y, gamma):
    X1_x, X1_y = X1[:, :-1], X1[:, -1]
    X2_x, X2_y = X2[:, :-1], X2[:, -1]
    g = mykernel_jax(gamma, X1_x, X1_y, locs_x, locs_y)
    h = mykernel_jax(gamma, X2_x, X2_y, locs_x, locs_y)
    Z = g - h
    return Z


def mykernel_jax(sigma, X, y, T_x, T_y):
    # X = X / sigma
    #
    # D2 = (X ** 2).sum(1).reshape((-1, 1)) - 2 * X.dot(T_x.T) + jnp.sum(T_x ** 2,
    #                                                                    1).reshape(
    #     (1, -1))
    # K1 = jnp.exp(-D2)

    X1 = X / sigma
    T_x = T_x / sigma

    D2 = (X ** 2).sum(1).reshape((-1, 1)) - 2 * X1.dot(T_x.T) + jnp.sum(T_x ** 2,
                                                                          1).reshape(
        (1, -1))
    K1 = jnp.exp(-0.5 * D2)

    # kernel on labels
    v1, v2 = jnp.meshgrid(y.ravel(), T_y.ravel(), indexing='ij')
    K2 = jnp.array(v1 == v2, dtype=jnp.int32)

    return K1 * K2

