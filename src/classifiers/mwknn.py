from collections import defaultdict
from collections import deque
import warnings
import numpy as np
warnings.simplefilter(action='ignore', category=FutureWarning)
import pandas as pd

from classifiers.utils import sort


class MultiWindowKnn:
    """

    Reference
    ----------

    Eleftherios Spyromitros-Xioufis, Myra Spiliopoulou, Grigorios Tsoumakas,
    and Ioannis Vlahavas. 2011. Dealing with concept drift and class imbalance in
    multi-label stream classification. In Twenty-Second International Joint Conference on
    Artificial Intelligence.
    """

    def __init__(self, n_p: int, k: int, threshold_window: int,
                 min_label_threshold: float,
                 max_label_threshold: float,
                 window_distribution_ratio: float):

        assert threshold_window < n_p
        assert 0 < window_distribution_ratio < 1
        assert 0 < min_label_threshold < 1
        assert 0 < max_label_threshold < 1

        self.k = k
        self.n_p = n_p
        self.window_distribution_ratio = window_distribution_ratio
        self.threshold_window = threshold_window
        self.min_label_threshold = min_label_threshold
        self.max_label_threshold = max_label_threshold

        self.classes = list()
        self.buffer = None
        self.Qp = dict()
        self.Qn = dict()
        self.sample_counter = pd.Series(dtype=int)
        self.total_sample_counter = 0
        self.label_frequency = defaultdict(int)
        self.label_confidence = defaultdict(list)
        self.label_threshold = defaultdict(lambda : self.min_label_threshold)
        self.counter_to_update_threshold = 0

    def fit_one(self, xt, yt):
        self.store_confidence(xt)

        xt_index = self.total_sample_counter
        self.sample_counter.loc[xt_index] = 0
        if self.buffer is None:
            self.buffer = pd.DataFrame(np.atleast_2d(xt))
        else:
            self.buffer.loc[xt_index] = xt

        self.counter_to_update_threshold += 1
        for y in yt:
            self.label_frequency[y] += 1
        if self.counter_to_update_threshold >= self.threshold_window: # **
            self._compute_threshold()

        yt = np.array(list(yt))
        for cl in np.union1d(self.classes, yt):
            if cl in np.intersect1d(self.classes, yt):
                self.Qp[cl] = self._update_windows(xt_index, self.Qp[cl])
            elif cl in np.setdiff1d(self.classes, yt):
                self.Qn[cl] = self._update_windows(xt_index, self.Qn[cl])
            else:
                self._add_new_class(cl)
                self.Qp[cl] = self._update_windows(xt_index, self.Qp[cl])

        self.total_sample_counter += 1

    def store_confidence(self, xt):
        if self.buffer is not None:
            _, confidence = self.predict_one(xt, return_confidence=True)
            for key, conf in confidence.items():
                self.label_confidence[key].append(conf)

    def _add_new_class(self, cl):
        self.classes.append(cl)
        self.Qp[cl] = deque(maxlen=self.n_p)
        self.Qn[cl] = deque(maxlen=round(self.n_p * self.window_distribution_ratio))

    def _update_windows(self, xt, queue):
        if len(queue) == queue.maxlen:
            self._pop_element_from_queue(queue)

        queue.append(xt)
        self.sample_counter.loc[xt] += 1
        return queue

    def _pop_element_from_queue(self, queue):
        sample_id = queue.popleft()
        self._remove_sample_from_counter(sample_id)

    def _remove_sample_from_counter(self, elem_id):
        if self.sample_counter[elem_id] > 1:
            self.sample_counter[elem_id] -= 1
        else:
            self.buffer.drop(index=elem_id, inplace=True, axis=0)
            self.sample_counter.drop(index=elem_id, inplace=True)

    def predict_one(self, xt, return_confidence=False):
        totalNNCounter = len(self.classes) * self.k
        NNCounter = {c: self.k for c in self.classes}
        votes = defaultdict(int)
        BSort_indexes = sort(self.buffer, xt)
        for index in BSort_indexes.index:
            if totalNNCounter == 0:
                break
            for cl in self.classes:
                if NNCounter[cl] == 0:
                    continue
                if index in np.union1d(
                        np.array(self.Qp[cl], dtype=int),
                        np.array(self.Qn[cl], dtype=int)):
                    NNCounter[cl] -= 1
                    totalNNCounter -= 1
                    if index in self.Qp[cl]:
                        votes[cl] += 1

        y_pred = set()
        confidence = dict()
        for cl in self.classes:
            confidence[cl] = votes[cl] / self.k
            if confidence[cl] >= self.label_threshold[cl]:
                y_pred.add(cl)
        if return_confidence:
            return y_pred, confidence
        else:
            return y_pred

    def _compute_threshold(self):
        for label, frq in self.label_frequency.items():
            confidences = list(set(self.label_confidence[label]))
            confidences.sort()
            prev_conf = 0
            for i, conf in enumerate(confidences):
                if conf > (frq / self.threshold_window):
                    break
                prev_conf = conf

            if prev_conf == conf:
                self.label_threshold[label] = conf
            else:
                thres = (conf + prev_conf) / 2
                self.label_threshold[label] = round(
                    min(max(thres, self.min_label_threshold), self.max_label_threshold),
                    2)

        self.label_frequency = defaultdict(int)
        self.label_confidence = defaultdict(list)
        self.counter_to_update_threshold = 0

    def get_class_indicator(self, window_size):
        dataset = self.buffer.copy(deep=True)
        n_features = len(dataset.columns)
        limit = self.total_sample_counter - window_size
        cls = self.classes.copy()
        cls.remove('root')
        all_indexes = list()
        for cl in cls:
            dataset[cl] = -1
            indexes = list(filter(lambda x: x >= limit, list(self.Qp[cl])))
            dataset.loc[indexes, cl] = 1
            all_indexes.extend(indexes)

        indicator_matrix = dataset.loc[set(all_indexes)].to_numpy()[:, n_features:]

        return indicator_matrix, cls
