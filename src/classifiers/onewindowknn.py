from sklearn.multioutput import MultiOutputClassifier
from sklearn.neighbors import KNeighborsClassifier


class OneWindowKnn:
    """ Knn with one sliding window """

    def __init__(self, k: int, window_size: int):
        self.window_size = window_size
        self._knn = MultiOutputClassifier(
            KNeighborsClassifier(n_neighbors=k), n_jobs=1)

    def fit(self, X, y):
        training_set_start = max(0, len(X) - self.window_size)
        self._knn.fit(X[training_set_start:], y[training_set_start:])

    def predict(self, x):
        return self._knn.predict(x)
