import numpy as np
from sklearn.gaussian_process.kernels import RBF


def mmd_is_same_distribution(old_window, new_window, threshold):
    """
    maximum mean discrepancy

    Reference
    ---------

    Gretton, Arthur, et al. "A kernel two-sample test." The Journal of Machine Learning
    Research 13.1 (2012): 723-773.
    """

    if old_window.shape != new_window.shape:
        raise RuntimeError(f'Old window and new window have different size '
                           f'{old_window.shape} != {new_window.shape}')

    sigma = sigma_heuristic(np.vstack([old_window[:, :-1], new_window[:, :-1]]))

    old = _kernel_x(sigma, old_window)
    new = _kernel_x(sigma, new_window)
    old_new = _kernel_x_y(sigma, old_window, new_window)
    mmd = old + new - old_new

    return mmd < threshold, mmd, sigma


def _kernel_x(sigma, window):
    kernel_mx = kernel(sigma, window, None)
    if np.max(kernel_mx) > 1:
        raise RuntimeError('kernel > 1')
    return np.sum(kernel_mx) / (len(window) * (len(window) - 1))


def _kernel_x_y(sigma, old_window, new_window):
    kernel_mx = kernel(sigma, old_window, new_window)
    if np.max(kernel_mx) > 1:
        raise RuntimeError('kernel > 1')
    return (2 * np.sum(kernel_mx)) / (len(old_window) * len(new_window))


def kernel(sigma, old, new):
    old_x = old[:, :-1]
    old_y = old[:, -1]
    if new is None:
        new_x = old[:, :-1]
        new_y = old[:, -1]
    else:
        new_x = new[:, :-1]
        new_y = new[:, -1]

    # kernel on features
    k = RBF(sigma)
    K1 = k(old_x, new_x)

    # kernel on labels
    v1, v2 = np.meshgrid(old_y, new_y, indexing='ij')
    couples_with_positive_label = np.array((v1 == v2) & (v1 == 1), dtype=int)
    couples_with_negative_label = np.array((v1 == v2) & (v1 == 0), dtype=int)
    if new is None:
        # remove diagonal
        a = np.ones_like(couples_with_negative_label)
        np.fill_diagonal(a, 0)
        couples_with_negative_label = couples_with_negative_label * a
        couples_with_positive_label = couples_with_positive_label * a

    weight_neg = 1 * couples_with_negative_label
    weight_pos = 1 * couples_with_positive_label

    K2 = weight_neg + weight_pos
    return K1 * K2


def sigma_heuristic(X):
    diff = [aa - bb for i, aa in enumerate(X) for e, bb in enumerate(X) if i != e]
    diff = np.array(diff)
    return np.median(np.linalg.norm(diff, axis=1))
