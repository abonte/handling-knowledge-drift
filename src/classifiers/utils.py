import numpy as np


def sort(dataset, xt):
    distances = dataset.apply(distance, axis=1, args=(xt,))
    return distances.sort_values(ascending=True)


def distance(x1, x2):
    # TODO to check
    return np.abs(np.linalg.norm(x1.values - x2))
