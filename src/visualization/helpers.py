import numpy as np
import seaborn as sns
from sklearn.metrics import precision_score, recall_score
from sklearn.preprocessing import MultiLabelBinarizer

from data.stream_generator import TypeOfDrift

METRIC_LABEL = {
    'f1': 'F$_1$',
    'hamming_loss': 'Hamming loss',
    'f1_micro': 'F$_{1-micro}$'
}

DRIFT_LABEL = {
    'concept_drift': 'concept drift',
    'add_implication': 'relation addition',
    'remove_implication': 'relation removal',
    'remove_concept': 'concept removal',
    'all_drifts': 'multi-drifts'
}


def _get_fig_caption(drifts):
    caption = 'Drifts:'
    for key, value in drifts.items():
        if value[0] in [TypeOfDrift.ADD_IMPL, TypeOfDrift.REMOVE_IMPL]:
            caption += f' iter: {key} subgraph: {value[1]} | '
        else:
            caption += f' iter: {key} label: {value[1]} | '
    return caption


def plot_drifts(ax, drifts):
    for key, value in drifts.items():
        if value[0] is TypeOfDrift.REMOVE_CONCEPT:
            ax.axvline(key, color='black')
        elif value[0] is TypeOfDrift.CONCEPT_DRIFT:
            ax.axvline(key, color='black')
        elif value[0] is TypeOfDrift.REMOVE_IMPL:
            ax.axvline(key, color='black')
        elif value[0] is TypeOfDrift.ADD_IMPL:
            ax.axvline(key, color='black')
        else:
            ax.axvline(key, color='black')
    return ax


def plot_detected_drifts(ax, detected_drifts, clf_name):
    iterations = (i for i, e in enumerate(detected_drifts) if e)
    for it in iterations:
        ax.axvline(it, color=get_color(clf_name), linestyle="--")
    return ax


def get_name(name):
    name_to_display, _ = _get_name_and_color(name)
    return name_to_display


def get_color(name):
    _, color = _get_name_and_color(name)
    return color


def _get_name_and_color(name):
    current_palette = sns.color_palette()
    if name == 'knn':
        return r'$k$NN', current_palette[0]
    elif name == 'knn_single_window':
        return '$k$NN 1 window', current_palette[6]
    elif name == 'our_knn_single_window':
        return 'kNN 1 window + adapt', current_palette[1]
    elif name == 'ourmethod':
        return 'TRCKD', current_palette[3]
    elif name == 'knn_xioufis':
        return 'MW-$k$NN', current_palette[4]
    elif name == 'dummy_clf':
        return name, 'black'
    elif name == 'punitive_knn':
        return 'PAW-$k$NN', current_palette[5]
    elif name == 'ourmethod_perfect_forget':
        return 'TRCKD_forget', current_palette[7]
    elif name == 'ourmethod_perfect':
        return 'TRCKD_oracle', 'green'
    elif name == 'ourmethod_no_interaction':
        return 'TRCKD_ni', current_palette[1]
    elif name == 'ourmethod_user_confirm':
        return 'TRCKD_nt', current_palette[6]
    elif name == 'ourmethod_automatic_identification':
        return 'TRCKD_LLR', current_palette[9]
    else:
        return name, 'black'


def get_zorder(clf_name):
    if clf_name == 'ourmethod':
        return 4
    elif clf_name == 'knn_xioufis':
        return 2
    elif clf_name == 'ourmethod_perfect':
        return 3
    elif clf_name == 'ourmethod_automatic_identification':
        return 2
    else:
        return 1


def compute_relation_precision_recall(true, estimated):
    precision = []
    recall = []
    # return precision, recall
    for tr, est in zip(true, estimated):
        if tr is not None and est is not None:
            mlb = MultiLabelBinarizer()
            binary = mlb.fit_transform([tr, est])
            precision.append(precision_score(binary[0], binary[1]))
            recall.append(recall_score(binary[0], binary[1]))
        else:
            precision.append(np.nan)
            recall.append(np.nan)
    return precision, recall


def plot_relation_precision_recall(ax, precision, recall, label=None):
    sns.lineplot(ax=ax,
                 y=precision['average'],
                 x=list(range(len(precision['average']))),
                 label=f'precision {label}'
                 )

    ax.fill_between(x=list(range(len(precision['average']))),
                    y1=precision['average'] - precision['std_error'],
                    y2=precision['average'] + precision['std_error'],
                    alpha=0.2)

    sns.lineplot(ax=ax,
                 y=recall['average'],
                 x=list(range(len(recall['average']))),
                 label=f'recall {label}'
                 )

    ax.fill_between(x=list(range(len(recall['average']))),
                    y1=recall['average'] - recall['std_error'],
                    y2=recall['average'] + recall['std_error'],
                    alpha=0.2)
    return ax
