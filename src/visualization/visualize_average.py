import logging
import os
import pickle
from collections import defaultdict
from os.path import join as pjoin
import numpy as np
from visualization.helpers import *
import seaborn as sns
import matplotlib.pyplot as plt

logging.basicConfig(level=logging.INFO)


def load_pickle(path):
    with open(path, 'rb') as f:
        obj = pickle.load(f)
    return obj


def load_all_statistics(save_path):
    list_stat = list()
    for file in os.listdir(save_path):
        if '_iteration_stats.pickle' in file and 'param_selection' not in file:
            list_stat.append(load_pickle(pjoin(save_path, file)))
    return list_stat


def compute_average(list_stat):
    logging.info('Compute average')
    methods = [met for met in list_stat[0]['metrics'].keys()]
    metrics = [col for col in list_stat[0]['metrics'][methods[0]].columns]

    average_stat = defaultdict(dict)

    for metric_name in metrics:
        if metric_name in ['drift_details']:
            continue
        for met in methods:
            collector = []
            for stat in list_stat:
                collector.append(stat['metrics'][met][metric_name].values)
            collector = np.array(collector)

            average_stat[metric_name][met] = dict()
            if np.all(collector == None) or None in collector:
                continue

            if metric_name == 'drift_detected':
                average_stat[metric_name][met]['sum'] = np.sum(collector, axis=0)
                average_stat[metric_name][met]['average'] = np.mean(
                    np.sum(collector, axis=1))
                average_stat[metric_name][met]['std_error'] = np.std(
                    np.sum(collector, axis=1)) / np.sqrt(
                    collector.shape[0])
            else:
                average_stat[metric_name][met]['average'] = np.mean(collector, axis=0)
                average_stat[metric_name][met]['std_error'] = np.std(collector,
                                                                     axis=0) / np.sqrt(
                    collector.shape[0])

    average_stat['drifts'] = list_stat[0]['drift_history']

    cardinalities_collector = list()
    for stat in list_stat:
        cardinalities_collector.append(stat['cardinality'])
    average_stat['cardinalities'] = cardinalities_collector

    precision, recall = get_precision_recall_average(list_stat,
                                                     'trckd_detected_relations')
    average_stat['trckd_relation_precision']['average'] = precision[0]
    average_stat['trckd_relation_precision']['std_error'] = precision[1]
    average_stat['trckd_relation_recall']['average'] = recall[0]
    average_stat['trckd_relation_recall']['std_error'] = recall[1]

    return dict(average_stat)


def get_precision_recall_average(list_stat, estimated_key):
    precision = list()
    recall = list()
    for stat in list_stat:
        prec, rec = compute_relation_precision_recall(stat['true_relations'],
                                                      stat[estimated_key]
                                                      )
        precision.append(prec)
        recall.append(rec)

    precision = np.array(precision)
    recall = np.array(recall)

    precision_mean = np.mean(precision, axis=0)
    precision_std_error = np.std(precision, axis=0) / np.sqrt(
        precision.shape[0])

    recall_mean = np.mean(recall, axis=0)
    recall_std_error = np.std(recall, axis=0) / np.sqrt(
        recall.shape[0])

    return (precision_mean, precision_std_error), (recall_mean, recall_std_error)


def plot_all_statistics(stat, save_path):
    logging.info('Plot...')
    plot_metrics(stat, save_path)
    plot_cardinality(stat, save_path)


def plot_cardinality(stat, save_path):
    plt.clf()
    for series in stat['cardinalities']:
        average = np.cumsum(series) / list(range(1, len(series) + 1))
        sns.lineplot(y=average, x=list(range(len(average))))
    plt.savefig(pjoin(save_path, 'cardinalities.png'))
    plt.close()


def plot_metrics(stat, save_path):
    all_clfs = ['knn', 'knn_single_window', 'our_knn_single_window', 'knn_xioufis',
                'ourmethod', 'dummy_clf', 'punitive_knn', 'ourmethod_perfect',
                'ourmethod_automatic_identification']
    rq1 = ['ourmethod_perfect', 'ourmethod_perfect_forget',
           'knn_xioufis', 'punitive_knn', 'knn', 'knn_single_window']
    rq2 = ['ourmethod', 'ourmethod_perfect', 'ourmethod_automatic_identification',
           'ourmethod_no_interaction', 'ourmethod_user_confirm']
    for list_clfs, plot_name in zip([all_clfs, rq1, rq2], ['all', 'rq1', 'rq2']):
        _plot_metrics(stat, save_path, plot_name, list_clfs)


def _plot_metrics(stat, save_path, plot_name, clf_to_plot):
    fig, axes = plt.subplots(nrows=6, ncols=3, figsize=(25, 20), dpi=100)

    metrics_to_skip = ['drifts', 'drift_detected', 'cardinalities',
                       'relation_precision',
                       'relation_recall', 'trckd_relation_precision',
                       'trckd_relation_recall']
    metrics_to_plot = [m for m in stat.keys() if m not in metrics_to_skip]
    for ax, metric_name in zip(axes.flatten(), metrics_to_plot):
        ax = draw_metric_for_methods(ax, clf_to_plot, metric_name, stat)

        for clf_name in stat['drift_detected'].keys():
            if clf_name == 'ourmethod':
                plot_detected_drifts(ax, stat['drift_detected'][clf_name]['sum'],
                                     clf_name)

    ax = axes.flatten()[-2]
    ax = plot_drifts(ax, stat['drifts'])
    ax = plot_relation_precision_recall(ax, stat['relation_precision'],
                                        stat['relation_recall'])
    ax.set_title('Implications')
    ax.set_xlabel('Iterations')

    ax = axes.flatten()[-1]
    ax = plot_drifts(ax, stat['drifts'])
    ax = plot_relation_precision_recall(ax, stat['trckd_relation_precision'],
                                        stat['trckd_relation_recall'])
    ax.set_title('TRCKD automatic identification - implications')
    ax.set_xlabel('Iterations')

    fig.tight_layout()
    plt.savefig(pjoin(save_path, f'average_metrics_{plot_name}.png'))
    plt.close()


def draw_metric_for_methods(ax, clfs, metric_name, stat, lw=2, xylabel=True):
    ax.set_title(metric_name)
    if xylabel:
        ax.set_ylabel(METRIC_LABEL[
                          metric_name] if metric_name in METRIC_LABEL.keys() else metric_name)
        ax.set_xlabel('Iterations')
    ax = plot_drifts(ax, stat['drifts'])

    for clf_name, metric_value in stat[metric_name].items():
        if clf_name in clfs:
            if 'average' not in metric_value.keys():
                continue
            x = list(range(len(metric_value['average'])))
            y = metric_value['average']

            ax = sns.lineplot(ax=ax, y=y, x=x, label=get_name(clf_name),
                              color=get_color(clf_name),
                              linewidth=lw,
                              zorder=get_zorder(clf_name))
            ax.fill_between(x=x,
                            y1=y - metric_value['std_error'],
                            y2=y + metric_value['std_error'],
                            alpha=0.2,
                            color=get_color(clf_name), zorder=get_zorder(clf_name))
    return ax
