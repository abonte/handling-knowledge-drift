from os.path import join as pjoin

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from visualization.helpers import _get_fig_caption, get_color, get_name, \
    plot_drifts, plot_detected_drifts, compute_relation_precision_recall, \
    plot_relation_precision_recall


def plot_all_statistics(stat, save_path, cv_round):
    list_of_metrics = stat['metrics'][list(stat['metrics'])[0]].columns.to_list()
    list_of_metrics.remove('drift_detected')
    list_of_metrics.extend(
        ['mmd_per_label', 'rbf_length', 'implications', 'trckd_implications',
         'classes_support'])

    sns.set_style("whitegrid")
    fig, axes = plt.subplots(nrows=5, ncols=3, figsize=(27, 20))
    txt = _get_fig_caption(stat['drift_history'])
    plt.figtext(0.5, 0.00, txt, wrap=True, horizontalalignment='center', fontsize=20)

    flatten_axes = axes.flatten()
    for ax, metric_name in zip(flatten_axes, list_of_metrics):
        ax = plot_drifts(ax, stat['drift_history'])
        ax = draw_detected_drifts(ax, stat)

        if metric_name == 'mmd_per_label':
            plot_mmd_per_label(ax, stat)
        elif metric_name == 'rbf_length':
            ax = plot_rbf_length(ax, stat)
        elif metric_name in ['implications', 'trckd_implications']:
            plot_implications_metrics(ax, stat, metric_name)
        elif metric_name == 'classes_support':
            plot_classes_support(ax, stat)
        else:
            plot_method_metrics(ax, metric_name, stat)

        ax.set_title(metric_name)
        ax.set_ylabel(metric_name)
        ax.set_xlabel('Iterations')

    fig.tight_layout()
    plt.savefig(pjoin(save_path, cv_round + '_metrics.png'))
    plt.close()


def plot_method_metrics(ax, metric_name, stat):
    for clf_name, metrics in stat['metrics'].items():
        metric = np.array(metrics[metric_name].values)
        sns.lineplot(ax=ax, y=metric, x=list(range(len(metric))),
                     label=get_name(clf_name),
                     color=get_color(clf_name)
                     )


def plot_implications_metrics(ax, stat, metric_name):
    ax.set_title('Implications precision recall')
    ax.set_xlabel('Iterations')
    if metric_name == 'trckd_implications':
        key = 'trckd_detected_relations'
    else:
        # key = 'detected_relations'
        return
    precision, recall = compute_relation_precision_recall(stat['true_relations'],
                                                          stat[key])
    precision = {'average': precision, 'std_error': np.zeros_like(precision)}
    recall = {'average': recall, 'std_error': np.zeros_like(recall)}
    plot_relation_precision_recall(ax, precision, recall)


def plot_mmd_per_label(ax, stat):
    for clf_name, df in stat['mmd_per_label'].items():
        if clf_name == 'ourmethod':
            for cl in df.columns:
                sns.lineplot(ax=ax, y=df[cl].values, x=list(range(len(df[cl].values))),
                             label=cl)
    ax = ax.axhline(stat['drift_detection_threshold'])
    return ax


def plot_rbf_length(ax, stat):
    for clf_name, df in stat['kernel_param_per_label'].items():
        if clf_name == 'ourmethod':
            for cl in df.columns:
                sns.lineplot(ax=ax, y=df[cl].values, x=list(range(len(df[cl].values))),
                             label=f'{cl} sigma')
    return ax


def draw_detected_drifts(ax, stat):
    for method in stat['metrics'].keys():
        detected_drifts = stat['metrics'][method]['drift_detected']
        ax = plot_detected_drifts(ax, detected_drifts, method)
    return ax


def plot_classes_support(ax, stat):
    df = stat['trckd_classes_support']
    for cl in df.columns:
        sns.lineplot(ax=ax, y=df[cl].values, x=list(range(len(df[cl].values))),
                     label=cl)
    return ax
