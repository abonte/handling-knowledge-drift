import argparse
import os

import matplotlib.pyplot as plt
import seaborn as sns

from utils import save_as_json
from visualization.helpers import DRIFT_LABEL, METRIC_LABEL
from visualization.visualize_average import load_all_statistics, compute_average, \
    draw_metric_for_methods

clfs = dict(rq1=['ourmethod_perfect', 'ourmethod_perfect_forget',
                 'knn_xioufis', 'punitive_knn', 'knn', 'knn_single_window'],
            rq2=['ourmethod_automatic_identification', 'ourmethod_perfect',
                 'ourmethod', 'ourmethod_no_interaction'],
            rq3=['knn', 'knn_single_window', 'knn_xioufis', 'ourmethod', 'punitive_knn',
                 'ourmethod_perfect'],
            lasso=['ourmethod', 'ourmethod_automatic_identification'])

sns.set_style("whitegrid")


def _draw(exp_path, plot_name, plot_config, rqs, output_path):
    plt.rc('xtick', labelsize=plot_config['ax_tick_label_size'])
    plt.rc('ytick', labelsize=plot_config['ax_tick_label_size'])

    if not os.path.exists(exp_path):
        raise RuntimeWarning(f'{exp_path} does not exist!')

    exp_name = exp_path.split('/')[-1]
    type_of_change = exp_name.split('__')[2].split('drift=')[1]
    dataset = exp_name.split('__')[1].split('dataset=')[1]

    list_stats = load_all_statistics(exp_path)
    average_stat = compute_average(list_stats)

    for research_question in rqs:
        if research_question == 'rq3' and type_of_change != 'all_drifts':
            continue

        save_path = os.path.join(output_path, research_question + '_f1micro')
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        exp = f'{plot_name}_{type_of_change}'

        print(f'{exp_path} fig saved in {save_path}')

        clfs_to_plots = [k for k, v in average_stat['f1'].items() if
                         k in clfs[research_question]]

        clf_name = 'our_knn_single_window' if 'our_knn_single_window' in clfs_to_plots else 'ourmethod'
        if clf_name in average_stat['drift_detected'].keys():
            if 'sum' in average_stat['drift_detected'][clf_name]:
                drift_detected = average_stat['drift_detected'][clf_name]['sum']

        fig_tile = f'{plot_name}_{DRIFT_LABEL[type_of_change]}'

        figf1, axf1 = plt.subplots(nrows=1, ncols=1, figsize=plot_config['figsize'],
                                   dpi=plot_config['dpi'])

        # F1 MICRO
        axf1 = draw_metric_for_methods(axf1, clfs_to_plots,
                                       'f1_micro',
                                       average_stat,
                                       plot_config['line_width'],
                                       xylabel=False)
        if research_question not in ['rq3', 'lasso'] and dataset == 'hstagger':
            axf1.set_title(DRIFT_LABEL[type_of_change],
                           fontsize=plot_config['title_fontsize'])
        else:
            axf1.set_title('')

        AX_CONFIG = [
            # rq, dataset, ylim_bottom, ylim_top
            ('lasso', None, 0.81, 0.99),
            ('rq1', 'hstagger', 0.78, 1.01),
            ('rq1', 'emnist', 0.36, 0.66),
            ('rq1', '20ng', 0.35, 0.61),
            ('rq2', 'hstagger', 0.78, 1.01),
            ('rq2', 'emnist', 0.42, 0.66),
            ('rq2', '20ng', 0.35, 0.61)
        ]

        for rq, ds, ylim_bottom, ylim_top in AX_CONFIG:
            if research_question == rq and (dataset == ds or ds is None):
                axf1.set_ylim(ylim_bottom, ylim_top)

        # axf1 = plot_detected_drifts(axf1, drift_detected, clf_name)
        if dataset == '20ng' or research_question in ['rq3', 'lasso']:
            axf1.set_xlabel('Iterations', fontsize=plot_config['ax_label_fs'])
            # axf1.set_ylabel(METRIC_LABEL['f1_micro'], fontsize=plot_config['ax_label_fs'])

        ## LEGEND
        cond1 = dataset == 'hstagger' and type_of_change == 'add_implication' and research_question == 'rq1'
        cond2 = dataset == 'hstagger' and type_of_change == 'remove_concept' and research_question == 'rq2'
        cond3 = dataset == 'hstagger' and type_of_change == 'all_drifts' and research_question == 'rq3'

        if cond1 or cond2 or cond3:
            # axf1.legend(ncol=2, fontsize=plot_config['legend_fs'])
            fig_leg = plt.figure(figsize=(7, 1))
            ax_leg = fig_leg.add_subplot(111)
            # add the legend from the previous axes
            leg = ax_leg.legend(*axf1.get_legend_handles_labels(), ncol=6,
                                fontsize=plot_config[
                                    'legend_fs'])  # , nocolloc='center')
            # hide the axes frame and the x/y labels
            ax_leg.axis('off')
            for line in leg.get_lines():
                line.set_linewidth(7.0)

            fig_leg.tight_layout()
            fig_leg.savefig(os.path.join(save_path, 'legend.pdf'), bbox_inches='tight')
            axf1.get_legend().remove()
        elif research_question == 'lasso':
            axf1.legend(fontsize=plot_config['legend_fs'])
            l = axf1.get_legend()
            l.get_texts()[1].set_text('TRCKD+lasso')
            l.get_texts()[0].set_text('TRCKD')
        else:
            axf1.get_legend().remove()

        figf1.tight_layout()
        figf1.savefig(os.path.join(save_path, f'f1micro_{exp}.pdf'),
                      bbox_inches='tight')
        plt.close(figf1)

        save_as_json(get_drift_detected(average_stat['drift_detected']),
                     os.path.join(save_path, f'drifts_{exp}.json'))


def get_drift_detected(average_stat):
    output = {}
    for clf_name, stat in average_stat.items():
        output[clf_name] = {}
        output[clf_name]['average'] = stat['average']
        output[clf_name]['std_error'] = stat['std_error']
    return output


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('basename', type=str,
                        help='basename of the PDF plots')
    parser.add_argument('-rq', type=str, nargs='+',
                        help='comma-separated list of research questions (rq1, rq2, rq3, lasso, me)')
    parser.add_argument('-i', dest='input_path', type=str,
                        default=os.path.join('results'),
                        help='output folder')
    parser.add_argument('-o', dest='output_path', type=str,
                        default=os.path.join('reports', 'IJCAI21'),
                        help='output folder')
    args = parser.parse_args()

    plot_config = {
        'title_fontsize': 25,
        'legend_fs': 22,
        'ax_label_fs': 25,
        'ax_tick_label_size': 20,
        'line_width': 2,
        'dpi': None,
        'figsize': (7, 4)
    }

    _draw(args.input_path, args.basename, plot_config, args.rq, args.output_path)

    print('Done!')
