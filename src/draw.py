import argparse
import logging

import seaborn as sns

from visualization.visualize_average import load_all_statistics, compute_average, \
    plot_all_statistics

if __name__ == '__main__':
    fmt_class = argparse.ArgumentDefaultsHelpFormatter
    parser = argparse.ArgumentParser(formatter_class=fmt_class)
    parser.add_argument('path', help='path to the experiment folder')
    args = parser.parse_args()

    list_stats = load_all_statistics(args.path)
    average_stat = compute_average(list_stats)

    sns.set_style("whitegrid")
    plot_all_statistics(average_stat, args.path)
    logging.info('Done')
