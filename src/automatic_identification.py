from sklearn.covariance import GraphicalLassoCV, GraphicalLasso
import numpy as np


def automatic_relation_computation(clf, threshold, window_size=70, glasso=False):
    if glasso:
        couples, alpha, cov = get_relations_using_glasso(clf, window_size)
        classes_support = dict()
    else:
        threshold = np.inf if threshold == 'inf' else threshold
        couples, classes_support = get_relations_using_likelihood_ratio(clf, threshold,
                                                                        window_size)
        alpha = None
    return couples, alpha, classes_support


# ******************
# LIKELIHOOD RATIO
# ******************


def get_relations_using_likelihood_ratio(clf, threshold, window_size):
    couples = []
    classes = clf.classes.copy()
    classes.remove('root')
    all_examples = list()
    limit = clf.total_sample_counter - window_size
    examples = {}
    classes_support = {}
    for cl in classes:
        examples[cl] = list(filter(lambda x: x >= limit, list(clf.Qp[cl])))
        all_examples.extend(examples[cl])
        classes_support[cl] = len(examples[cl])

    all_examples = np.array(list(set(all_examples)))

    for i, cl1 in enumerate(classes):
        for cl2 in classes[i:]:
            if cl1 != cl2:
                couple1 = likelihood_ratio(examples[cl1], examples[cl2], all_examples)
                couple2 = likelihood_ratio(examples[cl2], examples[cl1], all_examples)

                if (couple1 > couple2 and couple1 > threshold) or couple1 == np.inf:
                    couples.append((cl1, cl2))  # (parent, child)

                elif (couple2 > couple1 and couple2 > threshold) or couple2 == np.inf:
                    couples.append((cl2, cl1))  # (parent, child)

    couples = add_childs_of_root(classes, couples)

    return couples, classes_support


def likelihood_ratio(par, child, all_example):
    if not len(child) or not len(par):
        return 0
    # P(child=T | parent = T)
    p_c_p = np.sum(np.in1d(par, child)) / len(par)
    not_par = np.in1d(all_example, par, invert=True)
    not_par = all_example[not_par]
    # P(child=T | parent = F)
    if len(not_par):
        p_c_not_p = np.sum(np.in1d(not_par, child)) / len(not_par)
    else:
        # P(child=T | parent = F) = 0
        p_c_not_p = 0

    # P(child=T | parent = T) / P(child=T | parent = F)
    if p_c_not_p == 0:
        ratio = np.inf
    else:
        ratio = p_c_p / p_c_not_p

    return ratio


def get_relations_using_glasso(clf, window_size):
    X, cls = clf.get_class_indicator(window_size)
    couple_without_direction, lasso_alpha, cov = get_relation_couple_lasso(X)
    emp_relations = []
    for cl1, cl2 in couple_without_direction:
        couple = get_relation_direction(X, cl1, cl2)
        if couple is not None:
            emp_relations.append((cls[couple[0]], cls[couple[1]]))

    emp_relations = add_childs_of_root(cls, emp_relations)

    return list(set(emp_relations)), lasso_alpha, cov


def get_relation_couple_lasso(X):
    try:
        g_lasso = GraphicalLassoCV(alphas=[0.35, 0.5, 1], mode='cd',
                                   max_iter=250, tol=1e-3, enet_tol=1e-3).fit(X)
    except Exception as e:
        print(e)
        print('floating and runtime warn')
        return list(), None, None
    couples = get_correlated_couples_from_precision(g_lasso.precision_)
    return couples, g_lasso.alpha_, g_lasso.covariance_


def get_correlated_couples_from_precision(precision):
    idxs = np.where(np.tril(precision, -1) > 0.001)
    couples = list()
    for cl1, cl2 in zip(idxs[0], idxs[1]):
        couples.append((cl1, cl2))
    return couples


def get_relation_direction(X, cl1, cl2):
    cl1_idx = np.where(X[:, cl1] == 1)
    cl2_idx = np.where(X[:, cl2] == 1)

    return get_direction(cl1, cl1_idx, cl2, cl2_idx)


def get_direction(cl1, cl1_idx, cl2, cl2_idx):
    p_cl1_given_cl2 = np.sum(np.in1d(cl2_idx, cl1_idx)) / len(cl2_idx)
    p_cl2_given_cl1 = np.sum(np.in1d(cl1_idx, cl2_idx)) / len(cl1_idx)
    if np.array_equal(cl1_idx, cl2_idx):
        if np.random.randint(2):
            return cl2, cl1
        else:
            return cl1, cl2
    # cl2 --> cl1
    elif p_cl1_given_cl2 < p_cl2_given_cl1:
        return cl2, cl1
    # cl1 --> cl2
    else:
        return cl1, cl2


# ******************
# RELATION MANAGEMENT
# ******************

def add_childs_of_root(cls, emp_relations):
    childes = [c for _, c in emp_relations]
    for cl in cls:
        if cl not in childes and cl != 'root':
            emp_relations.append(('root', cl))
    return emp_relations


def find_deleted_relations(previous, current):
    del_rel = list()
    for rel in previous:
        if rel not in current:
            del_rel.append(rel)
    return del_rel


def find_added_relations(previous, current):
    add_rel = list()
    for rel in current:
        if rel not in previous:
            add_rel.append(rel)
    return add_rel
