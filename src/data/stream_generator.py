from abc import ABC, abstractmethod
from enum import Enum

from sklearn.utils import check_random_state


class TypeOfDrift(Enum):
    CONCEPT_DRIFT = 'concept_drift'
    ADD_IMPL = 'add_implication'
    REMOVE_IMPL = 'remove_implication'
    REMOVE_CONCEPT = 'remove_concept'
    ALL_DRIFTS = 'all_drifts'


class StreamGenerator(ABC):

    def __init__(self, random_state: int,
                 drift_type: str,
                 drift_positions: list = None):

        self.rng = check_random_state(random_state)

        self.drift_positions = drift_positions

        if drift_type == TypeOfDrift.CONCEPT_DRIFT.value:
            self.type_of_drift = TypeOfDrift.CONCEPT_DRIFT
        elif drift_type == TypeOfDrift.REMOVE_CONCEPT.value:
            self.type_of_drift = TypeOfDrift.REMOVE_CONCEPT
        elif drift_type == TypeOfDrift.ADD_IMPL.value:
            self.type_of_drift = TypeOfDrift.ADD_IMPL
        elif drift_type == TypeOfDrift.REMOVE_IMPL.value:
            self.type_of_drift = TypeOfDrift.REMOVE_IMPL
        elif drift_type == TypeOfDrift.ALL_DRIFTS.value:
            self.type_of_drift = TypeOfDrift.ALL_DRIFTS
        else:
            raise ValueError(drift_type)

        self.sample_counter = 0
        self.drift_counter = 0

        self.drift_order = [TypeOfDrift.CONCEPT_DRIFT, TypeOfDrift.ADD_IMPL,
                            TypeOfDrift.REMOVE_IMPL, TypeOfDrift.REMOVE_CONCEPT]

    @abstractmethod
    def next_sample(self, allow_drift):
        pass

    @abstractmethod
    def get_test_set(self):
        pass

    @abstractmethod
    def transform(self, obj):
        pass

    @abstractmethod
    def get_class_binarizer(self):
        pass


def get_random_node_from_dag(dag, random_state):
    while True:
        label = random_state.choice(list(dag.nodes))
        if label != 'root':
            return label
