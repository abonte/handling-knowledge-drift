import networkx as nx
from sklearn.preprocessing import MultiLabelBinarizer

from data.stream_generator import StreamGenerator, TypeOfDrift
from sklearn.model_selection import train_test_split
import numpy as np
import pandas as pd

from utils import load_pickle


class NewsgroupsStreamGenerator(StreamGenerator):

    def __init__(self, random_state: int, p_known: int, drift_type: str = None,
                 test_set_size: int = None,
                 drift_positions: list = None):

        super().__init__(random_state, drift_type, drift_positions)

        obj = load_pickle('data/newsgroups+embeddings+pca@100.pickle')
        X_dataset, y_dataset = pd.DataFrame(obj['data']), pd.DataFrame(obj['target'])

        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(
            X_dataset, y_dataset, test_size=test_set_size, shuffle=True,
            random_state=self.rng)

        self.topics = dict(
            religion=['alt.atheism', 'soc.religion.christian', 'talk.religion.misc'],
            computers=['comp.graphics', 'comp.os.ms-windows.misc',
                       'comp.sys.ibm.pc.hardware',
                       'comp.sys.mac.hardware', 'comp.windows.x'],
            sport=['rec.autos', 'rec.motorcycles', 'rec.sport.baseball',
                   'rec.sport.hockey'],
            science=['sci.crypt', 'sci.electronics', 'sci.med', 'sci.space'],
            politics=['talk.politics.guns', 'talk.politics.mideast',
                      'talk.politics.misc', 'misc.forsale'],
            favorite_topics=set()
        )

        self.dag = nx.DiGraph()
        for k, _ in self.topics.items():
            self.dag.add_edge('root', k)

        self.categories = obj['target_names']
        self._all_labels = self.topics.keys()
        cls = set(self._all_labels)
        cls.add('root')
        self._mlb = MultiLabelBinarizer()
        self._mlb.fit([cls])

        self.subgraphs = []
        self.subgraphs_state = {}

        # self.topics['favorite_topics'] = set(self.rng.choice(self.categories, 5, replace=False).tolist())
        self.add_to_favorite_topics()
        
        if self.type_of_drift is TypeOfDrift.REMOVE_IMPL:
            self.add_to_favorite_topics()
            
        curr_y_test = pd.DataFrame(self._update_data_labels(y_dataset.to_numpy()))

        self.X_train, self.X_test, self.y_train, self.current_y_test = train_test_split(
            X_dataset, curr_y_test, test_size=test_set_size, shuffle=True,
            random_state=self.rng)

        self.y_test = y_dataset.loc[self.X_test.index].to_numpy()
        self.X_train = self.X_train.to_numpy()
        self.X_test = self.X_test.to_numpy()
        self.y_train = y_dataset.loc[self.y_train.index].to_numpy()
        self.current_y_test = self.current_y_test.to_numpy()

        self.sample_counter = 0
        self.drift_position_counter = 0
        self.drift_counter = 0

    def next_sample(self, allow_drift: bool = True):
        drift = None
        drift_now = True if self.drift_position_counter in self.drift_positions else False
        if drift_now and allow_drift:
            if self.type_of_drift is TypeOfDrift.ALL_DRIFTS:
                type_of_drift = self.drift_order[self.drift_counter]
                self.drift_counter += 1
            else:
                type_of_drift = self.type_of_drift

            drift = self.generate_drift(type_of_drift)
            if drift is not None:
                self.current_y_test = self._update_data_labels(self.y_test)

        x = self.X_train[self.sample_counter, :]
        cat_idx = np.argmax(self.y_train[self.sample_counter, :])

        y = self._get_plain_labels(self.categories[cat_idx])

        if allow_drift:
            self.drift_position_counter += 1

        self.sample_counter += 1

        return x, y, drift

    def _get_plain_labels(self, cl):
        classes = set()
        for topic, categories in self.topics.items():
            if cl in categories:
                classes.add(topic)
        classes.add('root')
        return classes

    def generate_drift(self, type_of_drift):
        if type_of_drift is TypeOfDrift.CONCEPT_DRIFT:
            subgraph_n = self.remove_from_favorite_topics()
            topic_not_to_add = self.subgraphs[subgraph_n][1][1]
            subgraph_n = self.add_to_favorite_topics(topic_not_to_add)
            return type_of_drift, 'favorite_topics'

            # available_cat = [t for t in self.categories if t not in self.topics['favorite_topics']]
            # favorite_topics = set(self.rng.choice())
            # return type_of_drift, 'favorite_topics'

        elif type_of_drift is TypeOfDrift.REMOVE_CONCEPT:
            topic_to_remove = self.get_random_category()
            while topic_to_remove in self.dag.successors('favorite_topics'):
                topic_to_remove = self.get_random_category()
            self.topics.pop(topic_to_remove)
            return type_of_drift, topic_to_remove

        elif type_of_drift is TypeOfDrift.ADD_IMPL:
            subgraph_n = self.add_to_favorite_topics()
            return type_of_drift, subgraph_n

        elif type_of_drift is TypeOfDrift.REMOVE_IMPL:
            subgraph_n = self.remove_from_favorite_topics()
            return type_of_drift, subgraph_n

        else:
            raise ValueError(f'{type_of_drift} does not exist!')

    def remove_from_favorite_topics(self):
        available_subg = [k for k, v in self.subgraphs_state.items() if v]
        subgraph_number = self.rng.choice(available_subg)
        topic_to_remove = self.subgraphs[subgraph_number][1][1]

        self.subgraphs_state[subgraph_number] = False
        self.topics['favorite_topics'] = self.topics['favorite_topics'].difference(self.topics[topic_to_remove])
        self.dag.remove_edge('favorite_topics', topic_to_remove)
        self.dag.add_edge('root', topic_to_remove)
        return subgraph_number

    def add_to_favorite_topics(self, topic_not_to_add=None):
        topic_to_add = self.get_random_category()
        while topic_to_add in self.dag.successors('favorite_topics') or topic_to_add == topic_not_to_add:
            topic_to_add = self.get_random_category()

        self.subgraphs.append(
            [('root', 'favorite_topics'), ('favorite_topics', topic_to_add)])
        subgraph_n = len(self.subgraphs) - 1
        self.subgraphs_state[subgraph_n] = True
        self.topics['favorite_topics'].update(self.topics[topic_to_add])
        self.dag.add_edge('favorite_topics', topic_to_add)
        self.dag.remove_edge('root', topic_to_add)
        return subgraph_n

    def get_random_category(self):
        return self.rng.choice(
            list(set(self.topics.keys()).difference({'favorite_topics'})))

    def get_test_set(self):
        return self.X_test, self.current_y_test

    def _update_data_labels(self, y_test):
        y = list()
        for onehoty in y_test:
            y.append(self._get_plain_labels(self.categories[np.argmax(onehoty)]))
        return self._mlb.transform(y)

    def transform(self, obj):
        return self._mlb.transform(obj)

    def get_class_binarizer(self):
        return self._mlb.classes_
