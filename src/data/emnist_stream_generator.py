import networkx as nx
import numpy as np
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.model_selection import train_test_split

from data.stream_generator import StreamGenerator, TypeOfDrift, get_random_node_from_dag
from utils import load_pickle


class EmnistStreamGenerator(StreamGenerator):

    def __init__(self, random_state: int, test_set_size: int, p_known: int,
                 drift_type: str = None, drift_positions: list = None):

        super().__init__(random_state, drift_type, drift_positions)

        self.drift_position_counter = 0

        obj = load_pickle('data/emnist-embeddings-vae/emnist-embeddings-vae.pickle')

        emnist_X_test, emnist_y_test = obj['test_embeddings'], obj['test_labels']
        self.X_train, self.y_train = obj['train_embeddings'], obj['train_labels']

        self.concept_drifts_for_letters = [
            ('A', 'a'),  # A --> a
            ('B', 'b'),  # B --> b
            ('E', 'e'),  # E --> e
            ('F', 'f'),  # F --> f
            ('G', 'g'),  # G --> g
            ('H', 'h'),  # H --> h
            ('N', 'n'),  # N --> n
            ('Q', 'q'),  # Q --> q
            ('R', 'r'),  # R --> r
            ('T', 't')  # T --> t
        ]

        self._concepts = dict()
        self._concepts['concept0'] = list(range(0, 15))
        self._concepts['concept1'] = list(range(15, 30))
        self._concepts['concept2'] = list(range(30, 47))
        for i in range(3, 5):
            self._concepts[f'concept{i}'] = list(self.rng.choice(47, 15, replace=False))

        if self.type_of_drift is TypeOfDrift.ALL_DRIFTS:
            self._concepts['conceptA'] = list(self.rng.choice(47, 15, replace=False))

            self._concepts['conceptB'] = list(self.rng.choice(47, 15, replace=False))

        self._leaf_concepts = {str(label): e for e, label in
                               enumerate(obj['class_names'])}

        intermediate_classes = list(self._concepts.keys())

        if self.type_of_drift is TypeOfDrift.CONCEPT_DRIFT:
            self._all_labels = ['A']
        else:
            self._all_labels = intermediate_classes

        cls = set(self._all_labels)
        cls.add('root')
        self._mlb = MultiLabelBinarizer()
        self._mlb.fit([cls])

        self.concept_drifts_state = {i: False for i in
                                     range(len(self.concept_drifts_for_letters))}

        if self.type_of_drift is TypeOfDrift.CONCEPT_DRIFT:
            value_A = self._leaf_concepts[self.concept_drifts_for_letters[0][0]]
            value_a = self._leaf_concepts[self.concept_drifts_for_letters[0][1]]
            value_B = self._leaf_concepts[self.concept_drifts_for_letters[1][0]]

            idx_A = np.where(self.y_train == value_A)[0]
            idx_a = np.where(self.y_train == value_a)[0]
            idx_B = np.where(self.y_train == value_B)[0]

            train_set_before = np.concatenate(
                (idx_A[:self.drift_positions[0]], idx_B[:self.drift_positions[0]]))
            self.rng.shuffle(train_set_before)
            train_set_before = train_set_before[
                               :self.drift_positions[0] + p_known]
            # train_set_before = idx_A[:200]

            train_set_after = np.concatenate(
                (idx_a[self.drift_positions[0]:], idx_B[self.drift_positions[0]:]))
            self.rng.shuffle(train_set_after)
            # train_set_after = idx_a[200:]

            current_train_idx = np.concatenate((train_set_before, train_set_after))

            idx_A_test = np.where(emnist_y_test == value_A)[0]
            idx_a_test = np.where(emnist_y_test == value_a)[0]
            idx_B_test = np.where(emnist_y_test == value_B)[0]

            current_test_idx = np.concatenate((idx_A_test, idx_a_test, idx_B_test))

            _, self.X_test, _, self.y_test = train_test_split(emnist_X_test[current_test_idx],
                                                              emnist_y_test[current_test_idx],
                                                              shuffle=True,
                                                              test_size=test_set_size,
                                                              stratify=emnist_y_test[current_test_idx])
            # self.current_test_idx = np.concatenate((idx_A_test, idx_a_test))
        else:
            current_train_idx = list(range(len(self.X_train)))
            self.rng.shuffle(current_train_idx)
            _, self.X_test, _, self.y_test = train_test_split(emnist_X_test, emnist_y_test,
                                                              shuffle=True,
                                                              test_size=test_set_size,
                                                              stratify=emnist_y_test)

        self.X_train = self.X_train[current_train_idx]
        self.y_train = self.y_train[current_train_idx]

        self.subgraphs = [
             [('root', 'concept0'), ('concept0', 'concept3')]] #, ('concept0', 'concept2')]]

        if self.type_of_drift is TypeOfDrift.REMOVE_IMPL or self.type_of_drift is TypeOfDrift.ALL_DRIFTS:
            initial_flat_dag = False
        else:
            initial_flat_dag = True

        self.dag = self._init_dag(set(self._all_labels), initial_flat_dag)
        if self.type_of_drift is TypeOfDrift.ALL_DRIFTS:
            self.subgraphs.append([('root', 'concept1'), ('concept1', 'conceptA'), ('concept1', 'conceptB')])
            self.subgraphs_state[1] = False

        self.current_y_test = self._update_test_set_y(self.y_test)

    def next_sample(self, allow_drift: bool = True):
        drift = None
        drift_now = True if self.drift_position_counter in self.drift_positions else False
        if drift_now and allow_drift:
            if self.type_of_drift is TypeOfDrift.ALL_DRIFTS:
                type_of_drift = self.drift_order[self.drift_counter]
                self.drift_counter += 1
            else:
                type_of_drift = self.type_of_drift

            drift = self.generate_drift(type_of_drift)

            if drift is not None:
                self.current_y_test = self._update_test_set_y(self.y_test)

        x = self.X_train[self.sample_counter, :]
        cl = self.y_train[self.sample_counter]

        y = self._get_plain_labels(cl)

        if allow_drift:
            self.drift_position_counter += 1

        self.sample_counter += 1

        return x, y, drift

    def _get_plain_labels(self, cl):
        classes = set()
        for label in self.dag.nodes:
            if label != 'root':
                concept_name = self.dag.nodes[label]['concept']
                if concept_name in self._concepts.keys():
                    concept = cl in self._concepts[concept_name]
                    ###concept = self._concepts[concept_name](cl)
                else:
                    concept = concept_name == cl

                if concept:
                    classes.add(label)

        implied_cls = self._get_implied_classes(classes)
        implied_cls.add('root')
        return classes.union(implied_cls)

    def _get_implied_classes(self, classes):
        implied_cls = set()
        for cl in classes:
            edges = list(nx.edge_dfs(self.dag, cl, orientation='reverse'))
            for l1, l2, _ in edges:
                implied_cls.add(l1)
                implied_cls.add(l2)
        return implied_cls

    def generate_drift(self, type_of_drift: TypeOfDrift):
        if type_of_drift is TypeOfDrift.CONCEPT_DRIFT and not self.type_of_drift is TypeOfDrift.ALL_DRIFTS:
            node_name, new_concept = self.concept_drifts_for_letters[0]
            self.dag.nodes[node_name]['concept'] = self._leaf_concepts[new_concept]
            return type_of_drift, 'A'

        elif type_of_drift is TypeOfDrift.CONCEPT_DRIFT and self.type_of_drift is TypeOfDrift.ALL_DRIFTS:
            label = get_random_node_from_dag(self.dag, self.rng)
            while True:
                new_concept = self.rng.choice(47, 15, replace=False)
                if len(np.intersect1d(new_concept, self._concepts[label])) <= len(self._concepts[label]) / 3:
                    self._concepts[label] = new_concept
                    return type_of_drift, label

        elif type_of_drift is TypeOfDrift.REMOVE_CONCEPT:
            if len(self.dag.nodes):
                label = get_random_node_from_dag(self.dag, self.rng)
                self._remove_label(label)
                return type_of_drift, label

        elif type_of_drift is TypeOfDrift.ADD_IMPL:
            subgraph = 0 if not self.type_of_drift is TypeOfDrift.ALL_DRIFTS else 1
            self.add_subgraph(subgraph)
            return type_of_drift, subgraph

        elif type_of_drift is TypeOfDrift.REMOVE_IMPL:
            self.remove_subgraph(0)
            return type_of_drift, 0

    def add_subgraph(self, subgraph_number):
        self.subgraphs_state[subgraph_number] = True
        nodes_to_add = set()
        for node1, node2 in self.subgraphs[subgraph_number]:
            if node1 != 'root':
                nodes_to_add.add(node1)
            nodes_to_add.add(node2)
        for node in nodes_to_add:
            self.dag.remove_edge('root', node)

        self.dag.update(self.subgraphs[subgraph_number])

    def remove_subgraph(self, subgraph_number):
        self.subgraphs_state[subgraph_number] = False
        for parent, child in self.subgraphs[subgraph_number]:
            if parent != 'root':
                self.dag.remove_edge(parent, child)
                self.dag.add_edge('root', child)

    def _remove_label(self, label: str):
        self._validate_label(label)
        self.dag.remove_node(label)  # TODO manage non-leaf nodes

    def _validate_label(self, *labels):
        checks = [label not in self._all_labels for label in labels]
        if any(checks):
            raise ValueError(f'Label doesn\'t exist')

    def get_n_samples(self):
        return len(self.X_train)

    def get_test_set(self):
        X, y = self.X_test, self.current_y_test
        return X, y

    def transform(self, obj):
        return self._mlb.transform(obj)

    def get_class_binarizer(self):
        return self._mlb.classes_

    def _init_dag(self, classes: set, flat: bool):
        dag = nx.DiGraph()
        if flat:
            for lb in classes:
                dag.add_edge('root', lb)

            self.subgraphs_state = {i: False for i in range(len(self.subgraphs))}
        else:
            inserted_classes = set()
            for sub in self.subgraphs:
                dag.add_edges_from(sub)
                for l1, l2 in sub:
                    inserted_classes.update({l1, l2})
            for cl in set(self._all_labels).difference(inserted_classes):
                dag.add_edge('root', cl)
            self.subgraphs_state = {i: True for i in range(len(self.subgraphs))}

        for node in dag.nodes:
            if node in self._leaf_concepts.keys():
                dag.nodes[node]['concept'] = self._leaf_concepts[node]
            else:
                dag.nodes[node]['concept'] = node
        return dag

    def _update_test_set_y(self, y_test):
        y = list()
        for label in y_test:
            y.append(self._get_plain_labels(label))
        return self._mlb.transform(y)
