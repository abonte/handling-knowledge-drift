from collections import namedtuple

import networkx as nx
import numpy as np
from sklearn.preprocessing import MultiLabelBinarizer

from data.stream_generator import StreamGenerator, TypeOfDrift, get_random_node_from_dag


class StaggerStreamGenerator(StreamGenerator):

    def __init__(self, random_state: int, p_known: int,
                 drift_type: str, drift_positions: list, test_set_size: int = None):

        super().__init__(random_state, drift_type, drift_positions)

        self.size = ['small', 'medium', 'large', 'micro']
        self.shape = ['circle', 'square', 'triangle', 'cube']
        self.color = ['red', 'blue', 'green', 'black']

        self._concepts_history = []

        self._concepts = []
        n_values = {0: len(self.size), 1: len(self.shape), 2: len(self.color)}

        Cond = namedtuple('Cond', 'att value')
        Concept = namedtuple('Concept', 'cond1 cond2')
        for att1 in range(3):
            self._concepts += [Concept(Cond(att1, i), Cond(att1, ii)) for i in
                               range(n_values[att1]) for ii in
                               range(i, n_values[att1]) if i != ii]

        self.subgraphs = [
            [('root', 'label0'), ('label0', 'label1'), ('label0', 'label2')]
        ]

        self.subgraphs_state = None
        # ==============

        if self.type_of_drift is TypeOfDrift.CONCEPT_DRIFT:
            n_classes = 1
        elif self.type_of_drift is TypeOfDrift.ALL_DRIFTS:
            n_classes = 6
        else:
            n_classes = 5

        self._all_labels = ['label' + str(i) for i in range(n_classes)]
        if self.type_of_drift is TypeOfDrift.REMOVE_IMPL or self.type_of_drift is TypeOfDrift.ALL_DRIFTS:
            initial_flat_dag = False
        else:
            initial_flat_dag = True

        self.dag = self._init_dag(list(self._all_labels)[:n_classes], initial_flat_dag)

        if self.type_of_drift is TypeOfDrift.ALL_DRIFTS:
            self.subgraphs.append(
                [('root', 'label3'), ('label3', 'label4'), ('label4', 'label5')])
            self.subgraphs_state[1] = False

        self._all_labels = set(self._all_labels)
        self._mlb = MultiLabelBinarizer()

        cls = self._all_labels.copy()
        cls.add('root')
        self._mlb.fit([cls])

        self.test_set_x, self.test_set_y = self._generate_test_set()

    def next_sample(self, allow_drift: bool = True):
        drift = None
        drift_now = True if self.sample_counter in self.drift_positions else False
        if drift_now:
            if self.type_of_drift is TypeOfDrift.ALL_DRIFTS:
                type_of_drift = self.drift_order[self.drift_counter]
                self.drift_counter += 1
            else:
                type_of_drift = self.type_of_drift

            drift = self._generate_drift(type_of_drift)

            if drift is not None:
                self.test_set_y = self._update_test_set_y(self.test_set_x)

        if allow_drift:
            self.sample_counter += 1

        size = self.rng.randint(len(self.size))
        shape = self.rng.randint(len(self.shape))
        color = self.rng.randint(len(self.color))

        x = np.array([size, shape, color])
        y = self._get_plain_labels(x)
        return x, y, drift

    def _generate_drift(self, type_of_drift: TypeOfDrift) -> tuple:
        if type_of_drift is TypeOfDrift.CONCEPT_DRIFT:
            label = get_random_node_from_dag(self.dag, self.rng)
            self._generate_concept_drift(label)
            return type_of_drift, label

        elif type_of_drift is TypeOfDrift.REMOVE_CONCEPT:
            label = get_random_node_from_dag(self.dag, self.rng)
            self._remove_label(label)
            return type_of_drift, label

        elif type_of_drift is TypeOfDrift.REMOVE_IMPL:
            available_subg = [k for k, v in self.subgraphs_state.items() if v]
            if len(available_subg):
                subgraph_number = self.rng.choice(available_subg)
                subgraph_number = 0
                self.remove_subgraph(subgraph_number)
                return type_of_drift, subgraph_number

        elif type_of_drift is TypeOfDrift.ADD_IMPL:
            available_subg = [k for k, v in self.subgraphs_state.items() if not v]
            if len(available_subg):
                subgraph_number = self.rng.choice(available_subg)
                subgraph_number = 1 if self.type_of_drift is TypeOfDrift.ALL_DRIFTS else 0
                self.add_subgraph(subgraph_number)
                return type_of_drift, subgraph_number

        else:
            raise ValueError(f'{type_of_drift} does not exist!')

    def _generate_concept_drift(self, label):
        while True:
            new_concept = self.rng.randint(len(self._concepts))
            if new_concept != self.dag.nodes[label]['concept']:
                self.dag.nodes[label]['concept'] = new_concept
                self._concepts_history.append(new_concept)
                return new_concept

    def _add_label(self, label: str):
        self._validate_label(label)
        self.dag.add_edge('root', label)
        self.dag.nodes[label]['concept'] = self.rng.randint(len(self._concepts))

    def _remove_label(self, label: str):
        self._validate_label(label)
        self.dag.remove_node(label)  # TODO manage non-leaf nodes

    def _get_plain_labels(self, x) -> set:
        classes = set()
        for label in self.dag.nodes:
            if label != 'root':
                concept = self._concepts[self.dag.nodes[label]['concept']]
                if x[concept.cond1.att] == concept.cond1.value or x[
                    concept.cond2.att] == concept.cond2.value:
                    classes.add(label)
        implied_cls = self._get_implied_classes(classes)
        classes.add('root')
        return classes.union(implied_cls)

    def _get_implied_classes(self, classes):
        implied_cls = set()
        for cl in classes:
            edges = list(nx.edge_dfs(self.dag, cl, orientation='reverse'))
            for l1, l2, _ in edges:
                implied_cls.add(l1)
                implied_cls.add(l2)
        return implied_cls

    def _validate_label(self, *labels):
        checks = [label not in self._all_labels for label in labels]
        if any(checks):
            raise ValueError(f'Label doesn\'t exist')

    def _generate_test_set(self):
        all_comb = [[x, y, z]
                    for x in range(len(self.size))
                    for y in range(len(self.shape))
                    for z in range(len(self.color))]
        test_set_y = self._update_test_set_y(all_comb)
        return np.array(all_comb), test_set_y

    def _update_test_set_y(self, X):
        y = list()
        for x in X:
            labels = self._get_plain_labels(x)
            y.append(labels)
        return self._mlb.transform(y)

    def get_test_set(self):
        return self.test_set_x, self.test_set_y

    def transform(self, obj):
        return self._mlb.transform(obj)

    def get_class_binarizer(self):
        return self._mlb.classes_

    def _init_dag(self, classes: list, is_flat: bool):
        dag = nx.DiGraph()
        if is_flat:
            for lb in classes:
                dag.add_edge('root', lb)

            self.subgraphs_state = {i: False for i in range(len(self.subgraphs))}
        else:
            inserted_classes = set()
            for sub in self.subgraphs:
                dag.add_edges_from(sub)
                for l1, l2 in sub:
                    inserted_classes.update({l1, l2})
            for cl in set(self._all_labels).difference(inserted_classes):
                dag.add_edge('root', cl)
            self.subgraphs_state = {i: True for i in range(len(self.subgraphs))}

        for node in dag.nodes:
            if node != 'root':
                dag.nodes[node]['concept'] = self.rng.randint(len(self._concepts))
                self._concepts_history.append(dag.nodes[node]['concept'])
        return dag

    def remove_subgraph(self, subgraph_number):
        self.subgraphs_state[subgraph_number] = False
        for lb1, lb2 in self.subgraphs[subgraph_number]:
            if lb1 != 'root':
                self.dag.remove_edge(lb1, lb2)
                self.dag.add_edge('root', lb2)

    def add_subgraph(self, subgraph_number):
        self.subgraphs_state[subgraph_number] = True
        nodes_to_add = set()
        for node1, node2 in self.subgraphs[subgraph_number]:
            if node1 != 'root':
                nodes_to_add.add(node1)
            nodes_to_add.add(node2)
        for node in nodes_to_add:
            self.dag.remove_edge('root', node)

        self.dag.update(self.subgraphs[subgraph_number])
