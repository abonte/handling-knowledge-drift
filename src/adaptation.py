from automatic_identification import automatic_relation_computation
import networkx as nx

from automatic_identification import find_deleted_relations, find_added_relations
from data.stream_generator import TypeOfDrift as td

DEFAULT_RETURN_DICT = dict(is_drift_detected=False, stat_test_values=None,
                           kernel_params=None, detected_class=None, cov=None,
                           estimated_relation=None, classes_support=None)


def adaptation(clf, clf_name, drift_now, last_occurred_drift, stream,
               detected_last_drift, previous_rel=None, threshold=None,
               window_size=None, it=None, glasso=False):
    if glasso and it < 100:
        return DEFAULT_RETURN_DICT
    if not getattr(clf, "ADAPTATION", None):
        return DEFAULT_RETURN_DICT
    if 'ourmethod_perfect' == clf_name:
        return perfect_adaptation(clf, drift_now, stream)
    elif 'ourmethod_perfect_forget' == clf_name:
        return always_forget_adaptation(clf, drift_now, stream)
    elif '_no_interaction' in clf_name:
        return no_interaction_adaptation(clf)
    elif clf_name == 'ourmethod':
        return adaptation_normal(clf, last_occurred_drift, stream,
                                 detected_last_drift)
    elif clf_name == 'ourmethod_automatic_identification':
        return automatic_identification_adaptation(clf, previous_rel,
                                                   threshold,
                                                   window_size, glasso)
    else:
        raise RuntimeError(f'No adaptation for {clf_name}')


def perfect_adaptation(clf, drift_now, stream):
    if drift_now is not None:
        adapt_clf_to_drift(clf, drift_now, stream)
    return DEFAULT_RETURN_DICT


def always_forget_adaptation(clf, drift_now, stream):
    if drift_now is not None:
        adapt_clf_to_drift_always_forget(clf, drift_now, stream)
    return DEFAULT_RETURN_DICT


def no_interaction_adaptation(clf):
    result = clf.drift_detected()
    if result['is_drift_detected']:
        adapt_clf_to_drift_always_forget(clf, (td.CONCEPT_DRIFT, result['detected_classes'][0]),
                                         None)
    return result


def adaptation_normal(clf, last_occurred_drift, stream, detected_last_drift):
    result = clf.drift_detected()
    adapt = False
    if result['is_drift_detected']:
        if last_occurred_drift is not None and not detected_last_drift:
            if is_the_rigth_class_detected(last_occurred_drift, result['detected_classes'],
                                           stream):
                adapt = True
                adapt_clf_to_drift(clf, last_occurred_drift, stream)
    result['adapted'] = adapt
    return result


def is_the_rigth_class_detected(last_occurred_drift, classes_with_drift, stream):
    if last_occurred_drift[0] in [td.CONCEPT_DRIFT, td.REMOVE_CONCEPT]:
        return last_occurred_drift[1] in classes_with_drift
    elif last_occurred_drift[0] in [td.ADD_IMPL, td.REMOVE_IMPL]:
        subgraph = stream.subgraphs[last_occurred_drift[1]]
        for lb1, lb2 in subgraph:
            if (lb1 != 'root' and lb1 in classes_with_drift) or lb2 in classes_with_drift:
                return True
        return False
    else:
        raise RuntimeError


def propagate_addition_hierarchy(add_rel, estimated_rel):
    # order relations
    dag = nx.DiGraph()
    dag.add_edges_from(add_rel)
    ordered_rel = list(nx.edge_dfs(dag, orientation='reverse'))
    ordered_rel.reverse()
    return ordered_rel


def automatic_identification_adaptation(clf, previous_rel, threshold,
                                        window_size, glasso):
    estimated_rel, alpha_lasso, classes_support = automatic_relation_computation(
        clf, threshold=threshold, window_size=window_size, glasso=glasso)

    result = clf.drift_detected()
    result['estimated_relation'] = estimated_rel
    result['classes_support'] = classes_support

    if not result['is_drift_detected']:
        return result

    if previous_rel is not None:
        del_rel = find_deleted_relations(previous_rel, estimated_rel)

        for cl_implicated, cl in del_rel:
            if cl_implicated != 'root':
                clf.delete_relation(cl_implicated, cl)

        add_rel = find_added_relations(previous_rel, estimated_rel)
        add_rel = propagate_addition_hierarchy(add_rel, estimated_rel)

        for cl_implicated, cl, _ in add_rel:
            if cl_implicated != 'root':
                clf.add_relation(cl_implicated, cl)

        if len(del_rel) == 0 and len(add_rel) == 0:  # no changes in the hierarchy
            # assume concept drift happens
            for cl in result['detected_classes']:
                clf.adapt_to_concept_drift(cl)

    return result


def adapt_clf_to_drift_always_forget(clf, last_occurred_drift, stream):
    if last_occurred_drift is not None:
        if last_occurred_drift[0] in [td.CONCEPT_DRIFT, td.REMOVE_CONCEPT]:
            clf.adapt_to_concept_drift(last_occurred_drift[1])
        elif last_occurred_drift[0] in [td.ADD_IMPL, td.REMOVE_IMPL]:
            subgraph = stream.subgraphs[last_occurred_drift[1]]
            for lb1, lb2 in subgraph:
                if lb1 != 'root':
                    clf.adapt_to_concept_drift(lb1)


def adapt_clf_to_drift(clf, drift, stream):
    if drift[0] is td.CONCEPT_DRIFT:
        clf.adapt_to_concept_drift(drift[1])
    elif drift[0] is td.REMOVE_CONCEPT:
        clf.delete_class(drift[1])
    elif drift[0] is td.ADD_IMPL:
        for parent, child in stream.subgraphs[drift[1]]:
            if parent != 'root':
                clf.add_relation(parent, child)
    elif drift[0] is td.REMOVE_IMPL:
        for parent, child in stream.subgraphs[drift[1]]:
            if parent != 'root':
                clf.delete_relation(parent, child)
    else:
        raise RuntimeError
