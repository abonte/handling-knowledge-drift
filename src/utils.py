import json
import yaml
import pickle


def save_as_json(obj, path):
    with open(path, 'w') as f:
        json.dump(obj, f, indent=4)


def load_json(path):
    with open(path, 'r') as f:
        obj = json.load(f)
    return obj


def load_config(path):
    with open(path) as file:
        yaml_data = yaml.safe_load(file)
    return yaml_data


def save_as_pickle(obj, path):
    with open(path, 'wb') as f:
        pickle.dump(obj, f)


def load_pickle(path):
    with open(path, 'rb') as f:
        obj = pickle.load(f)
    return obj
