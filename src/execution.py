from collections import defaultdict

import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, precision_score, recall_score, hamming_loss, \
    f1_score, fbeta_score
from sklearn.multioutput import MultiOutputClassifier
from sklearn.neighbors import KNeighborsClassifier
from tqdm import tqdm

from adaptation import adaptation
from classifiers.mwknn import MultiWindowKnn
from classifiers.onewindowknn import OneWindowKnn
from classifiers.punitiveknn import PunitiveKnn
from classifiers.trckd import TRCKD
from data.emnist_stream_generator import EmnistStreamGenerator
from data.newsgroups_stream_generator import NewsgroupsStreamGenerator
from data.stagger_stream_generator import StaggerStreamGenerator
from data.stream_generator import TypeOfDrift


def compute_metrics_on_test_set(clf, stream, deleted_class=None):
    X_test, y_test_encoded = stream.get_test_set()
    if getattr(clf, 'predict', None):
        y_pred_test = clf.predict(X_test)
    elif getattr(clf, 'predict_one', None):
        y_pred_test = np.array([])
        for x in X_test:
            y_pred_encoded = stream.transform([clf.predict_one(x)])[0]
            y_pred_test = np.vstack([y_pred_test, y_pred_encoded]) \
                if y_pred_test.size else y_pred_encoded

    class_order = stream.get_class_binarizer().copy()

    # remove 'root' label from metric computation
    root_index = np.where(class_order == 'root')[0][0]
    class_order = np.delete(class_order, root_index)
    y_true = np.delete(y_test_encoded, root_index, 1)
    y_pred = np.delete(y_pred_test, root_index, 1)
    return _compute(y_true, y_pred, deleted_class, class_order)


def _compute(y_true, y_pred, deleted_class, class_order):
    average = 'binary' if y_true.shape[1] == 1 else 'samples'
    return dict(
        accuracy=my_accuracy(y_true, y_pred),
        exact_match=np.all(y_true == y_pred),
        precision=precision_score(y_true, y_pred, zero_division=0, average=average),
        recall=recall_score(y_true, y_pred, average=average, zero_division=0),
        hamming_loss=hamming_loss(y_true, y_pred),
        f1=f1_score(y_true, y_pred, average=average, zero_division=1),
        f1_micro=f1_score(y_true, y_pred, average='micro', zero_division=0),
        f1_macro=f1_score(y_true, y_pred, average='macro', zero_division=0),
        fbeta05=fbeta_score(y_true, y_pred, average='macro', beta=0.5, zero_division=0)
    )


def my_accuracy(y_true, y_pred):
    acc = []
    for i in range(y_true.shape[1]):
        acc.append(accuracy_score(y_true[:, i], y_pred[:, i]))
    return np.mean(acc)


def update_stat(stat, xt, yt, y_true_encoded, drift, i):
    for elem in yt:
        stat['n_classes'][elem] += 1

    stat['cardinality'] = np.append(stat['cardinality'], len(yt))
    stat['label_sets'].add(frozenset(yt))

    if drift is not None:
        stat['drift_history'][i] = drift

    stat['x'] = np.vstack([stat['x'], xt]) if stat['x'].size else xt
    stat['y_true'] = np.vstack([stat['y_true'], y_true_encoded]) if stat[
        'y_true'].size else y_true_encoded
    return stat


def bootstrap_phase(clfs, n_known, stat, stream):
    for i in range(n_known):
        xt1, yt1, _ = stream.next_sample(allow_drift=False)
        y_true_encoded = stream.transform([yt1])

        stat['x'] = np.vstack([stat['x'], xt1]) if stat['x'].size else xt1
        stat['y_true'] = np.vstack([stat['y_true'], y_true_encoded]) if stat[
            'y_true'].size else y_true_encoded

        for clf_name, clf in clfs.items():
            if getattr(clf, "fit_one", None) is not None:
                clf.fit_one(xt1, yt1)

    for clf_name, clf in clfs.items():
        if getattr(clfs[clf_name], "fit", None) is not None:
            clf.fit(stat['x'], stat['y_true'])
    return stat


DATASETS = {
    'hstagger': StaggerStreamGenerator,
    'emnist': EmnistStreamGenerator,
    '20ng': NewsgroupsStreamGenerator
}


def execution(config: dict, args, repeat_counter: int):
    config['drift_detector']['random_state'] = config['seed']
    np.random.seed(config['seed'])

    def get_TRCKD_instance(sample_to_keep):
        return TRCKD(
            k=config['k'],
            classifier_config=config['xioufis_based_methods'],
            detector_config=config['drift_detector'],
            sample_to_keep=sample_to_keep
        )

    all_clfs = dict(
        # Xioufis
        knn_xioufis=MultiWindowKnn(k=config['k'], **config['xioufis_based_methods']),

        ourmethod=get_TRCKD_instance(config['sample_to_keep']),
        ourmethod_automatic_identification=get_TRCKD_instance(config['sample_to_keep']),
        ourmethod_perfect=get_TRCKD_instance(0),
        ourmethod_perfect_forget=get_TRCKD_instance(0),
        ourmethod_no_interaction=get_TRCKD_instance(config['sample_to_keep']),

        punitive_knn=PunitiveKnn(**config['punitive_knn']),

        # KNN
        knn=MultiOutputClassifier(
            KNeighborsClassifier(n_neighbors=config['k']), n_jobs=1),
        knn_single_window=OneWindowKnn(
            k=config['k'],
            window_size=config['knn_single_window_size']
        ),
    )

    stat = dict(
        # for single method
        metrics=defaultdict(lambda: pd.DataFrame(dtype=float)),
        mmd_per_label=defaultdict(lambda: pd.DataFrame(dtype=float)),
        kernel_param_per_label=defaultdict(lambda: pd.DataFrame(dtype=float)),
        # data set
        x=np.array([]),
        y_true=np.array([]),
        cardinality=np.array([], dtype=int),
        label_sets=set(),
        n_classes=defaultdict(int),
        all_labels=None,
        drift_history=dict(),
        drift_detection_threshold=0,
        # run automatic detection
        true_relations=list(),
        trckd_detected_relations=[None],
        trckd_classes_support=pd.DataFrame(dtype=int),
    )

    # select predictor to be used in this experiment
    clfs = {k: v for k, v in all_clfs.items() if k in config['methods']}

    stream = DATASETS[args.dataset](random_state=config['seed'],
                                    test_set_size=args.test_size,
                                    drift_positions=args.drift_positions,
                                    p_known=args.p_known,
                                    drift_type=args.drift_type)

    stat = bootstrap_phase(clfs, args.p_known, stat, stream)

    skip_evaluation = args.drift_positions[0] - 25

    last_occurred_drift = None
    detected_last_drift = {n: True for n in clfs.keys()}
    seen_classes = set()
    subgraph_added = False
    previous_rel = None
    deleted_class = None

    progress_bar = tqdm(range(args.max_iters), ascii=True,
                        position=int(repeat_counter))
    progress_bar.set_description(f'run {repeat_counter}')

    for it in progress_bar:
        xt, yt, drift_now = stream.next_sample()

        if drift_now is not None:
            if drift_now[0] is TypeOfDrift.REMOVE_CONCEPT:
                deleted_class = drift_now[1]

        y_true_encoded = stream.transform([yt])
        stat = update_stat(stat, xt, yt, y_true_encoded, drift_now, it)
        seen_classes.update(yt)

        stat['true_relations'].append(list(stream.dag.edges))

        if drift_now is not None:
            last_occurred_drift = drift_now
            detected_last_drift = {n: False for n in clfs.keys()}

        if it <= args.drift_positions[0] - 5:
            previous_rel = list(stream.dag.edges)

        for clf_name, clf in clfs.items():

            clf_stat = dict()
            if it > skip_evaluation:
                clf_stat = compute_metrics_on_test_set(clf, stream, deleted_class)

            if it == skip_evaluation:  # 98
                if isinstance(clf, TRCKD) and clf_name not in ['ourmethod_perfect',
                                                               'ourmethod_perfect_forget']:
                    if config['drift_detector']['test'] == 'ME':
                        clf.optimize()

            adaptation_output = adaptation(clf, clf_name, drift_now,
                                           last_occurred_drift,
                                           stream, detected_last_drift[clf_name],
                                           previous_rel=previous_rel,
                                           threshold=config['automatic_identification'][
                                               'likelihood_ratio_threshold'],
                                           window_size=config['drift_detector'][
                                               'window'],
                                           it=it,
                                           glasso=config['automatic_identification'][
                                                      'type'] == 'graphical_lasso'
                                           )

            if clf_name == 'ourmethod_automatic_identification':
                stat['trckd_detected_relations'].append(
                    adaptation_output['estimated_relation'])
                stat['trckd_classes_support'] = stat['trckd_classes_support'].append(
                    adaptation_output['classes_support'], ignore_index=True)

            if adaptation_output['is_drift_detected']:
                if clf_name == 'ourmethod' and not adaptation_output['adapted'] and not \
                        detected_last_drift[clf_name]:
                    detected_last_drift[clf_name] = False
                else:
                    detected_last_drift[clf_name] = True
                if clf_name == 'ourmethod_automatic_identification':
                    previous_rel = adaptation_output['estimated_relation']

            # store stats
            clf_stat['drift_detected'] = adaptation_output['is_drift_detected']
            if getattr(clf, "ADAPTATION", None):
                stat['mmd_per_label'][clf_name] = stat['mmd_per_label'][
                    clf_name].append(adaptation_output['stat_test_values'],
                                     ignore_index=True)

                stat['kernel_param_per_label'][clf_name] = \
                    stat['kernel_param_per_label'][clf_name].append(
                        adaptation_output['kernel_params'], ignore_index=True)

            stat['metrics'][clf_name] = stat['metrics'][clf_name].append(clf_stat,
                                                                         ignore_index=True)

        # fit predictor
        for clf_name, clf in clfs.items():
            if getattr(clfs[clf_name], "fit_one", None):
                clf.fit_one(xt, yt)
            elif getattr(clfs[clf_name], "fit", None):
                clf.fit(stat['x'], stat['y_true'])

        # update queue size when "remove implication" experiment
        if not subgraph_added and args.drift_type == 'remove_implication':
            if 'ourmethod' in clfs.keys() or 'ourmethod_perfect' in clfs.keys():
                if len(seen_classes) >= len(stream._all_labels):
                    if all([v for k, v in stream.subgraphs_state.items()]):
                        subgraph_added = True
                        for subgraph in stream.subgraphs:
                            for parent, child in subgraph:
                                if parent != 'root':
                                    for name, classifier in clfs.items():
                                        if 'ourmethod' in name:
                                            clfs[name].increase_class_window_size(
                                                parent)

    # end of experiment
    progress_bar.close()
    stat['all_labels'] = stream.get_class_binarizer()
    stat['config'] = config

    stat['drift_detection_threshold'] = 0
    if 'ourmethod' in clfs.keys():
        stat['drift_detection_threshold'] = clfs['ourmethod']._threshold

    if args.dataset == 'hstagger':
        stat['concept_history'] = stream._concepts_history

    return stat
